# Collabora OpenXR System Tests

**NOTE:** These tests were developed independently at Collabora,
in the context of testing runtime development.
They are not official conformance tests or otherwise associated with
the Khronos OpenXR working group.

## Requirements

This requires:

- C++14
- The OpenXR loader and headers.
- the Microsoft C++ GSL header library (Debian package libmsgsl-dev, vcpkg package ms-gsl)
- for OpenGL session testing:
  - OpenGL
  - glfw3
  - On Linux, xlib headers
- Optionally: xrtraits (a vendored copy is included as a submodule, which will be used if not found elsewhere).

## Building on Linux

Something like the following will configure, build, and run it,
assuming you have a sibling `OpenXR-SDK` directory with a nested build tree in a `build` directory.
(Finding the loader is currently the hairiest part, since there's no standard for where to install it on Linux.)

```sh
mkdir build
cd build
cmake .. -G Ninja -DCMAKE_BUILD_TYPE=RelWithDebInfo
# Can append something like this to the CMake line to force CTest to use a given runtime:
#-DXR_RUNTIME_JSON=/home/ryan/src/monado/build/openxr_monado-dev.json

ninja

# Either this, for easiest output readability
tests/xrst-test-SystemTest # set any env variables you need
# or this, in case e.g. your editor has a CTest integration or you otherwise prefer it
ctest # Can't be parallel!
```

## Building on Windows

Should build with MSVC2017, known to build with 2019.

If it gets stuck looking for the loader, just set the CMake variable `OPENXR_loader_LIBRARY` to the loader library file.

To get the deps from vcpkg, do:

```bat
vcpkg install glfw3 ms-gsl
```

If you're using the fork at <https://github.com/rpavlik/vcpkg/tree/openxr>, you can tack `openxr-loader` to the end of that command,
then have no worries in the world.

Then, you can configure with something like the following, in the source directory (adjust paths to your environment, can add the `-DXR_RUNTIME_JSON` configuration option just like in the Linux instructions if required):

```bat
mkdir build
cd build
cmake .. -G "Visual Studio 16 2019" -DCMAKE_TOOLCHAIN_FILE=c:\Users\ryanp\src\third-party\vcpkg\scripts\buildsystems\vcpkg.cmake

rem Now, build the project from Visual Studio, or do something like this line:
cmake --build . --config RelWithDebInfo

rem Then, to run, either this (after setting any environment variables you need), for easiest output readability
tests/RelWithDebInfo/xrst-test-SystemTest

rem or this, in case e.g. your editor has a CTest integration or you otherwise prefer it - don't run it parallel!
ctest --config RelWithDebInfo
```

## Running

You can run the test binary directly (it's a normal Catch2 runner right now, pass `-h` for help), with something like the following,
after setting any environment variables you might need:

**Single-configuration generator (Ninja, makefiles, etc):**

```sh
tests/xrst-test-SystemTest
```

**Multi-configuration generator (MS Visual Studio, etc):**

```sh
tests/RelWithDebInfo/xrst-test-SystemTest
```

Alternately, you may use CTest to trigger the tests.
If using CTest, the value of `XR_RUNTIME_JSON` supplied to CMake, if any, will be used to set environment variables before each run.
*Be sure not to run tests in parallel unless you're really ready for that.*

**Single-configuration generator (Ninja, makefiles, etc):**

```sh
ctest
```

**Multi-configuration generator (MS Visual Studio, etc):**

```sh
ctest --config RelWithDebInfo
```

## License

Code written originally for this project is licensed under the Boost Standard License v1.0.
Some other files, including dependencies as well as files originating in other projects, are Apache-2.0.

Please see each file for details: All files added for this project specifically should
have an SPDX tag in them.
