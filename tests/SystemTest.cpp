// Copyright 2018-2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  General system tests
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

// Internal Includes
#include <util/Instance.h>
#include <util/SystemTestUtils.h>

// Library Includes
// - none

// Standard Includes
#include <string>

using namespace xrtraits;
using namespace std::string_literals;
using Catch::Matchers::VectorContains;

TEST_CASE("setup_headless")
{
	// Create instance
	auto inst = makeInstance({}, {XR_MND_HEADLESS_EXTENSION_NAME});
	XrInstance instance = inst->getInstance();
	REQUIRE(instance != XR_NULL_HANDLE_CPP);
	auto apiLayerProps = CHECK_TWO_CALL(XrExtensionProperties, xrEnumerateInstanceExtensionProperties, nullptr);
	AND_WHEN("Getting instance props")
	{
		Initialized<XrInstanceProperties> instance_props;
		CHECK_RESULT(XR_SUCCESS, xrGetInstanceProperties(instance, &instance_props));
	}

	// Get system
	XrSystemId systemId;
	{
		Initialized<XrSystemGetInfo> system_get_info;
		system_get_info.formFactor = XR_FORM_FACTOR_HEAD_MOUNTED_DISPLAY;
		REQUIRE_RESULT(XR_SUCCESS, xrGetSystem(instance, &system_get_info, &systemId));
	}
	AND_WHEN("Getting system properties")
	{
		Initialized<XrSystemProperties> system_properties;
		CHECK_RESULT(XR_SUCCESS, xrGetSystemProperties(instance, systemId, &system_properties));

		CHECK_FALSE(system_properties.systemName[0] == '\0');
		CHECK(system_properties.graphicsProperties.maxSwapchainImageHeight > 0);
		CHECK(system_properties.graphicsProperties.maxSwapchainImageWidth > 0);
		CHECK(system_properties.graphicsProperties.maxLayerCount > 0);
		CHECK_THAT(system_properties.trackingProperties.orientationTracking, In<XrBool32>({XR_TRUE, XR_FALSE}));
		CHECK_THAT(system_properties.trackingProperties.positionTracking, In<XrBool32>({XR_TRUE, XR_FALSE}));
	}

	const auto viewConfigType = XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO;
	const auto numViews = 2;
	AND_WHEN("Enumerating view configs")
	{
		auto viewConfigTypes =
		    REQUIRE_TWO_CALL(XrViewConfigurationType, xrEnumerateViewConfigurations, instance, systemId);
		REQUIRE_FALSE(viewConfigTypes.empty());

		THEN("All must be known")
		{
			for (const auto& vctype : viewConfigTypes) {
				REQUIRE_THAT(vctype, In<XrViewConfigurationType>({XR_VIEW_CONFIGURATION_TYPE_PRIMARY_MONO,
				                                                  XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO}));
			}
		}
	}
	auto viewConfigViews = REQUIRE_TWO_CALL(XrViewConfigurationView, xrEnumerateViewConfigurationViews, instance,
	                                        systemId, viewConfigType);
	{
		REQUIRE(viewConfigViews.size() == numViews);

		Initialized<XrViewConfigurationProperties> props;
		REQUIRE_RESULT(XR_SUCCESS, xrGetViewConfigurationProperties(instance, systemId, viewConfigType, &props));
		REQUIRE(props.viewConfigurationType == viewConfigType);
	}

	AND_WHEN("Creating a session")
	{
		XrSession session = XR_NULL_HANDLE_CPP;
		{
			Initialized<XrSessionCreateInfo> session_create_info;
			session_create_info.systemId = systemId;

			REQUIRE_RESULT(XR_SUCCESS, xrCreateSession(instance, &session_create_info, &session));
			REQUIRE_FALSE(XR_NULL_HANDLE_CPP == session);
		}
		auto cleanupSession = gsl::finally([&] { CHECK_RESULT(XR_SUCCESS, xrDestroySession(session)); });

		AND_WHEN("Calling CreateReferenceSpace with nonexistant reference space type")
		{
			XrSpace localSpace = XR_NULL_HANDLE_CPP;
			Initialized<XrReferenceSpaceCreateInfo> reference_space_create_info;
			reference_space_create_info.referenceSpaceType = XR_REFERENCE_SPACE_TYPE_MAX_ENUM;
			reference_space_create_info.poseInReferenceSpace.orientation = {0, 0, 0, 1};
			REQUIRE_RESULT(XR_ERROR_REFERENCE_SPACE_UNSUPPORTED,
			               xrCreateReferenceSpace(session, &reference_space_create_info, &localSpace));
			REQUIRE(localSpace == XR_NULL_HANDLE_CPP);
		}
		AND_WHEN("Calling CreateReferenceSpace with non-unit quat")
		{
			XrSpace localSpace = XR_NULL_HANDLE_CPP;
			Initialized<XrReferenceSpaceCreateInfo> reference_space_create_info;
			reference_space_create_info.referenceSpaceType = XR_REFERENCE_SPACE_TYPE_LOCAL;
			reference_space_create_info.poseInReferenceSpace.orientation = {1, 1, 1, 1};
			REQUIRE_RESULT(XR_ERROR_POSE_INVALID,
			               xrCreateReferenceSpace(session, &reference_space_create_info, &localSpace));
			REQUIRE(localSpace == XR_NULL_HANDLE_CPP);
		}
		AND_WHEN("Calling CreateReferenceSpace with zero quat")
		{
			XrSpace localSpace = XR_NULL_HANDLE_CPP;
			Initialized<XrReferenceSpaceCreateInfo> reference_space_create_info;
			reference_space_create_info.referenceSpaceType = XR_REFERENCE_SPACE_TYPE_LOCAL;
			// zero-init quaternion is invalid
			REQUIRE_RESULT(XR_ERROR_POSE_INVALID,
			               xrCreateReferenceSpace(session, &reference_space_create_info, &localSpace));
			REQUIRE(localSpace == XR_NULL_HANDLE_CPP);
		}

		// Actually create a space
		AND_WHEN("Correctly calling CreateReferenceSpace")
		{
			XrSpace localSpace = XR_NULL_HANDLE_CPP;
			{
				Initialized<XrReferenceSpaceCreateInfo> reference_space_create_info;
				reference_space_create_info.referenceSpaceType = XR_REFERENCE_SPACE_TYPE_LOCAL;
				reference_space_create_info.poseInReferenceSpace.orientation = {0, 0, 0, 1};
				REQUIRE_RESULT(XR_SUCCESS, xrCreateReferenceSpace(session, &reference_space_create_info, &localSpace));
				REQUIRE_FALSE(localSpace == XR_NULL_HANDLE_CPP);
			}
			auto cleanupLocalSpace = gsl::finally([&] { CHECK_RESULT(XR_SUCCESS, xrDestroySpace(localSpace)); });

			AND_WHEN("Calling EndSession before BeginSession")
			{
				CHECK_RESULT(XR_ERROR_SESSION_NOT_RUNNING, xrEndSession(session));
			}
			AND_WHEN("Calling BeginFrame before BeginSession")
			{
				CHECK_RESULT(XR_ERROR_SESSION_NOT_RUNNING, xrBeginFrame(session, nullptr));
			}
			AND_WHEN("Calling EndFrame before BeginSession")
			{
				Initialized<XrFrameEndInfo> desc;
				CHECK_RESULT(XR_ERROR_SESSION_NOT_RUNNING, xrEndFrame(session, &desc));
			}
			AND_WHEN("Calling BeginSession")
			{
				// Actually begin session
				Initialized<XrSessionBeginInfo> sessionBeginInfo;
				REQUIRE_RESULT(XR_SUCCESS, xrBeginSession(session, &sessionBeginInfo));

				AND_WHEN("Calling BeginSession a second time")
				{
					REQUIRE_RESULT(XR_ERROR_SESSION_RUNNING, xrBeginSession(session, &sessionBeginInfo));
				}
				AND_WHEN("Calling EndFrame before BeginFrame")
				{
					Initialized<XrFrameEndInfo> desc;
					CHECK_RESULT(XR_ERROR_CALL_ORDER_INVALID, xrEndFrame(session, &desc));
				}
#if 0
				AND_WHEN("Creating actions")
				{
					XrActionSet inGameActionSet = XR_NULL_HANDLE_CPP;
					{
						Initialized<XrActionSetCreateInfo> actionSetInfo;
						assignString(actionSetInfo.actionSetName) = "gameplay";
						CHECK_RESULT(XR_SUCCESS, xrCreateActionSet
						             (session, &actionSetInfo, &inGameActionSet));
						CHECK_FALSE(inGameActionSet == XR_NULL_HANDLE_CPP);
					}

					// create a "teleport" input action
					XrAction teleportAction = XR_NULL_HANDLE_CPP;
					{
						Initialized<XrActionCreateInfo> actioninfo;
						assignString(actioninfo.actionName) = "teleport";
						actioninfo.actionType = XR_INPUT_ACTION_TYPE_BOOLEAN;
						assignString(actioninfo.localizedActionName) = "Teleport";

						XrPath devicePath = XR_NULL_PATH;
						REQUIRE_RESULT(
						    XR_SUCCESS, xrStringToPath,
						    (instance, "/user/hand/primary/input/1/click", &devicePath));
						actioninfo.suggestedBindings = &devicePath;
						actioninfo.suggestedBindingCount = 1;

						REQUIRE_RESULT(XR_SUCCESS, xrCreateAction
						               (inGameActionSet, &actioninfo, &teleportAction));
						CHECK_FALSE(teleportAction == XR_NULL_HANDLE_CPP);
					}

					// create a "player_hit" output action
					XrAction hapticsAction = XR_NULL_HANDLE_CPP;
					{
						Initialized<XrActionCreateInfo> actioninfo;
						assignString(actioninfo.actionName) = "player_hit";
						actioninfo.actionType = XR_OUTPUT_ACTION_TYPE_VIBRATION;
						assignString(actioninfo.localizedActionName) = "Player hit";

						XrPath devicePath = XR_NULL_PATH;
						REQUIRE_RESULT(
						    XR_SUCCESS, xrStringToPath,
						    (instance, "/user/hand/primary/output/haptic", &devicePath));
						actioninfo.suggestedBindings = &devicePath;
						actioninfo.suggestedBindingCount = 1;
						REQUIRE_RESULT(XR_SUCCESS, xrCreateAction
						               (inGameActionSet, &actioninfo, &hapticsAction));
						CHECK_FALSE(hapticsAction == XR_NULL_HANDLE_CPP);
					}
					AND_WHEN("Adding an action with no suggested bindings")
					{
						Initialized<XrActionCreateInfo> actioninfo;
						assignString(actioninfo.actionName) = "dummy";
						actioninfo.actionType = XR_INPUT_ACTION_TYPE_BOOLEAN;
						assignString(actioninfo.localizedActionName) = "Dummy";

						XrAction tempAction = XR_NULL_HANDLE_CPP;
						REQUIRE_RESULT(XR_SUCCESS, xrCreateAction
						               (inGameActionSet, &actioninfo, &tempAction));
						CHECK_FALSE(tempAction == XR_NULL_HANDLE_CPP);
					}
					AND_WHEN("Querying boolean action state")
					{
						Initialized<XrActionStateBoolean> teleportState;
						REQUIRE_RESULT(XR_SUCCESS, xrGetActionStateBoolean
						               (teleportAction, &teleportState));
						//! @todo see #535 https://gitlab.khronos.org/openxr/openxr/issues/535
						CHECK(teleportState.isInActiveSet == XR_FALSE);
					}
					AND_WHEN("Querying Vector1f state of Boolean action")
					{
						Initialized<XrActionStateVector1f> state;
						REQUIRE_RESULT(XR_ERROR_ACTION_TYPE_MISMATCH, xrGetActionStateVector1f
						               (teleportAction, &state));
					}
					AND_WHEN("Querying Vector2f state of Boolean action")
					{
						Initialized<XrActionStateVector2f> state;
						REQUIRE_RESULT(XR_ERROR_ACTION_TYPE_MISMATCH, xrGetActionStateVector2f
						               (teleportAction, &state));
					}
					AND_WHEN("Querying Vector3f state of Boolean action")
					{
						Initialized<XrActionStateVector3f> state;
						REQUIRE_RESULT(XR_ERROR_ACTION_TYPE_MISMATCH, xrGetActionStateVector3f
						               (teleportAction, &state));
					}
					AND_WHEN("Querying Pose state of Boolean action")
					{
						Initialized<XrActionStatePose> state;
						REQUIRE_RESULT(XR_ERROR_ACTION_TYPE_MISMATCH, xrGetActionStatePose
						               (teleportAction, &state));
					}
					AND_WHEN("Querying Boolean state of haptic action")
					{
						Initialized<XrActionStateBoolean> state;
						REQUIRE_RESULT(XR_ERROR_ACTION_TYPE_MISMATCH, xrGetActionStateBoolean
						               (hapticsAction, &state));
					}
					AND_WHEN("Sending haptics output to Boolean action")
					{
						Initialized<XrHapticVibration> vibration;
						vibration.amplitude = 0.5;
						vibration.duration = 300;
						vibration.frequency = 3000;
						REQUIRE_RESULT(
						    XR_ERROR_ACTION_TYPE_MISMATCH, xrApplyHapticFeedback,
						    (teleportAction,
						     base_header_cast<const XrHapticBaseHeader*>(vibration)));
					}
					AND_WHEN("Sending haptics output")
					{
						// fire haptics using output action
						Initialized<XrHapticVibration> vibration;
						vibration.amplitude = 0.5;
						vibration.duration = 300;
						vibration.frequency = 3000;
						REQUIRE_RESULT(
						    XR_SUCCESS, xrApplyHapticFeedback,
						    (hapticsAction,
						     base_header_cast<const XrHapticBaseHeader*>(vibration)));
					}
					AND_WHEN("Syncing action sets")
					{
						Initialized<XrSyncActionDataInfo> syncInfo;
						syncInfo.activeActionSets = &inGameActionSet;
						syncInfo.numActiveActionSets = 1;
						REQUIRE_RESULT(XR_SUCCESS, xrSyncActionData (session, &syncInfo));
					}
				}
#endif
			}
		}
	}
}
