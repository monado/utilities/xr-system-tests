// Copyright 2018-2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Testing passing an unrecognized structure type
 *
 * @todo generate this for every call it applies to...
 *
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#include <util/SessionGenerator.h>
#include <util/SystemTestUtils.h>

//! @todo find a way to guarantee this is not assigned.
constexpr XrStructureType UnrecognizedStructureType = static_cast<XrStructureType>(14);

static inline XrBaseInStructure createUnrecognizedInput(const void* next = nullptr)
{
	return {UnrecognizedStructureType, reinterpret_cast<const struct XrBaseInStructure*>(next)};
}

static inline XrBaseOutStructure createUnrecognizedOutput(void* next = nullptr)
{
	return {UnrecognizedStructureType, reinterpret_cast<struct XrBaseOutStructure*>(next)};
}

TEST_CASE("xrGetSystem_types")
{
	auto generator = graphicalSessionGenerator();
	while (generator.next()) {
		auto&& sessCreator = generator.get();
		DYNAMIC_SECTION("Using a session described as " << sessCreator.getSessionDescription())
		{

			auto sessEtAl = sessCreator.invoke(XR_FORM_FACTOR_HEAD_MOUNTED_DISPLAY);
			XRS_GET_PARTS_FROM_SESSION_ET_AL(sessEtAl);
			WHEN("calling xrGetSystem")
			{
#if 0
                // Think this isn't actually permitted by the spec.
				AND_WHEN("passing an unrecognized structure at the head of the chain")
				{
					Initialized<XrSystemGetInfo> system_get_info{XR_FORM_FACTOR_HEAD_MOUNTED_DISPLAY};
					auto unrecognized = createUnrecognizedInput(&system_get_info);

					XrSystemId systemId = XR_NULL_SYSTEM_ID;
					REQUIRE_RESULT(
					    XR_SUCCESS,
					    xrGetSystem(instance, reinterpret_cast<const XrSystemGetInfo*>(&unrecognized), &systemId));
					REQUIRE_FALSE(systemId == XR_NULL_SYSTEM_ID);
				}
#endif
				AND_WHEN("passing an unrecognized structure at the end of the chain")
				{
					auto unrecognized = createUnrecognizedInput();
					XrSystemGetInfo system_get_info{
					    XR_TYPE_SYSTEM_GET_INFO,
					    &unrecognized,
					    XR_FORM_FACTOR_HEAD_MOUNTED_DISPLAY,
					};

					XrSystemId systemId = XR_NULL_SYSTEM_ID;
					REQUIRE_RESULT(XR_SUCCESS, xrGetSystem(instance, &system_get_info, &systemId));
					REQUIRE_FALSE(systemId == XR_NULL_SYSTEM_ID);
				}
				//! @todo for calls that expect two things in the chain, check unrecognized-in-middle.
			}
		}
	}
}
