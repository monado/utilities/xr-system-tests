// Copyright 2018-2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  OpenGL system tests
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#include <util/Config.h>
#include <util/Instance.h>
#include <util/OpenGL.h>
#include <util/XrPlatformInclude.h>

#include <string>
#include <util/SystemTestUtils.h>

using namespace std::string_literals;
using Catch::Matchers::VectorContains;

TEST_CASE("setup_opengl")
{
#ifdef XR_USE_GRAPHICS_API_OPENGL
	// Create instance
	auto inst = makeInstance({}, {XR_KHR_OPENGL_ENABLE_EXTENSION_NAME});
	XrInstance instance = inst->getInstance();

	// Get system
	XrSystemId systemId;
	{
		Initialized<XrSystemGetInfo> system_get_info;
		system_get_info.formFactor = XR_FORM_FACTOR_HEAD_MOUNTED_DISPLAY;
		REQUIRE_RESULT(XR_SUCCESS, xrGetSystem(instance, &system_get_info, &systemId));
	}
	AND_WHEN("Getting system properties")
	{
		Initialized<XrSystemProperties> system_properties;
		CHECK_RESULT(XR_SUCCESS, xrGetSystemProperties(instance, systemId, &system_properties));

		CHECK_FALSE(system_properties.systemName[0] == '\0');
		CHECK(system_properties.graphicsProperties.maxSwapchainImageHeight > 0);
		CHECK(system_properties.graphicsProperties.maxSwapchainImageWidth > 0);
		CHECK(system_properties.graphicsProperties.maxLayerCount > 0);
		CHECK_THAT(system_properties.trackingProperties.orientationTracking, In<XrBool32>({XR_TRUE, XR_FALSE}));
		CHECK_THAT(system_properties.trackingProperties.positionTracking, In<XrBool32>({XR_TRUE, XR_FALSE}));
	}

	const auto viewConfigType = XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO;
	const auto numViews = 2;
	WHEN("Enumerating view configs")
	{
		auto viewConfigTypes =
		    REQUIRE_TWO_CALL(XrViewConfigurationType, xrEnumerateViewConfigurations, instance, systemId);

		REQUIRE_FALSE(viewConfigTypes.empty());

		THEN("All must be known")
		{
			for (const auto& vctype : viewConfigTypes) {
				REQUIRE_THAT(vctype, In<XrViewConfigurationType>({XR_VIEW_CONFIGURATION_TYPE_PRIMARY_MONO,
				                                                  XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO}));
			}
		}
		REQUIRE_THAT(viewConfigTypes, VectorContains<XrViewConfigurationType>(viewConfigType));
	}

	auto viewConfigViews = REQUIRE_TWO_CALL(XrViewConfigurationView, xrEnumerateViewConfigurationViews, instance,
	                                        systemId, viewConfigType);
	{
		REQUIRE(viewConfigViews.size() == numViews);

		Initialized<XrViewConfigurationProperties> props;
		REQUIRE_RESULT(XR_SUCCESS, xrGetViewConfigurationProperties(instance, systemId, viewConfigType, &props));
		REQUIRE(props.viewConfigurationType == viewConfigType);
	}

	AND_WHEN("Trying to create session without graphics binding struct")
	{
		Initialized<XrSessionCreateInfo> session_create_info;
		session_create_info.systemId = systemId;
		XrSession session = XR_NULL_HANDLE_CPP;

		REQUIRE_RESULT(XR_ERROR_GRAPHICS_DEVICE_INVALID, xrCreateSession(instance, &session_create_info, &session));
	}
	AND_WHEN("Creating session")
	{
		auto sessionInterface = makeOpenGLSession(instance, systemId);
		XrSession session = sessionInterface->get();
		// Actually create a space
		AND_WHEN("Correctly calling CreateReferenceSpace")
		{
			XrSpace localSpace = XR_NULL_HANDLE_CPP;
			{
				Initialized<XrReferenceSpaceCreateInfo> reference_space_create_info;
				reference_space_create_info.referenceSpaceType = XR_REFERENCE_SPACE_TYPE_LOCAL;
				reference_space_create_info.poseInReferenceSpace.orientation = {0, 0, 0, 1};
				REQUIRE_RESULT(XR_SUCCESS, xrCreateReferenceSpace(session, &reference_space_create_info, &localSpace));
				REQUIRE_FALSE(localSpace == XR_NULL_HANDLE_CPP);
			}
			auto cleanupLocalSpace = gsl::finally([&] { CHECK_RESULT(XR_SUCCESS, xrDestroySpace(localSpace)); });

			AND_WHEN("Calling EndSession before BeginSession")
			{
				CHECK_RESULT(XR_ERROR_SESSION_NOT_RUNNING, xrEndSession(session));
			}
			AND_WHEN("Calling WaitFrame before BeginSession")
			{
				Initialized<XrFrameWaitInfo> frameWaitInfo;
				Initialized<XrFrameState> frameState;
				CHECK_RESULT(XR_ERROR_SESSION_NOT_RUNNING, xrWaitFrame(session, &frameWaitInfo, &frameState));
			}
			AND_WHEN("Calling BeginFrame before BeginSession")
			{
				CHECK_RESULT(XR_ERROR_SESSION_NOT_RUNNING, xrBeginFrame(session, nullptr));
			}
			AND_WHEN("Calling EndFrame before BeginSession")
			{
				Initialized<XrFrameEndInfo> desc;
				CHECK_RESULT(XR_ERROR_SESSION_NOT_RUNNING, xrEndFrame(session, &desc));
			}
			AND_WHEN("Calling BeginSession with null primaryViewConfigurationType")
			{
				Initialized<XrSessionBeginInfo> sessionBeginInfo;
				CHECK_RESULT(XR_ERROR_VIEW_CONFIGURATION_TYPE_UNSUPPORTED, xrBeginSession(session, &sessionBeginInfo));
			}

			AND_WHEN("Calling BeginSession")
			{
				// Actually begin session
				Initialized<XrSessionBeginInfo> sessionBeginInfo;
				sessionBeginInfo.primaryViewConfigurationType = viewConfigType;
				REQUIRE_RESULT(XR_SUCCESS, xrBeginSession(session, &sessionBeginInfo));

				AND_WHEN("Calling BeginSession a second time")
				{
					/// @todo what happens if beginSession is called at a
					/// bad time, *and* is also missing the view config?
					/// Which is the correct error code when two could
					/// apply?
					REQUIRE_RESULT(XR_ERROR_SESSION_RUNNING, xrBeginSession(session, &sessionBeginInfo));
				}
				AND_WHEN("Calling EndFrame before BeginFrame")
				{
					Initialized<XrFrameEndInfo> desc;
					CHECK_RESULT(XR_ERROR_CALL_ORDER_INVALID, xrEndFrame(session, &desc));
				}
				uint64_t format = 0;

				INFO("Enumerating swapchain formats");
				{
					auto formats = REQUIRE_TWO_CALL(int64_t, xrEnumerateSwapchainFormats, session);
					REQUIRE_FALSE(formats.empty());
					format = formats.front();
				}
				INFO("Creating swapchain");
				XrSwapchain swapchain = XR_NULL_HANDLE_CPP;
				{
					Initialized<XrSwapchainCreateInfo> swapchain_create_info;
					swapchain_create_info.usageFlags =
					    XR_SWAPCHAIN_USAGE_SAMPLED_BIT | XR_SWAPCHAIN_USAGE_COLOR_ATTACHMENT_BIT;
					swapchain_create_info.format = format; // just use the first enumerated format
					swapchain_create_info.sampleCount = 1;
					swapchain_create_info.width = viewConfigViews.front().recommendedImageRectWidth;
					swapchain_create_info.height = viewConfigViews.front().recommendedImageRectHeight;
					swapchain_create_info.faceCount = 1;
					swapchain_create_info.arraySize = 1;
					swapchain_create_info.mipCount = 1;
				}

				AND_WHEN("Calling WaitFrame, BeginFrame, EndFrame cycle twice")
				{
					{
						Initialized<XrFrameWaitInfo> frameWaitInfo;
						Initialized<XrFrameState> frameState;
						CHECK_RESULT(XR_SUCCESS, xrWaitFrame(session, &frameWaitInfo, &frameState));

						CHECK_FALSE(frameState.predictedDisplayTime == 0);
						CHECK_FALSE(frameState.predictedDisplayPeriod == 0);

						Initialized<XrFrameBeginInfo> beginDesc;
						CHECK_RESULT(XR_SUCCESS, xrBeginFrame(session, &beginDesc));

						Initialized<XrFrameEndInfo> endDesc;
						CHECK_RESULT(XR_SUCCESS, xrEndFrame(session, &endDesc));
					}

					{
						Initialized<XrFrameWaitInfo> frameWaitInfo;
						Initialized<XrFrameState> frameState;
						CHECK_RESULT(XR_SUCCESS, xrWaitFrame(session, &frameWaitInfo, &frameState));

						Initialized<XrFrameBeginInfo> beginDesc;
						CHECK_RESULT(XR_SUCCESS, xrBeginFrame(session, &beginDesc));

						Initialized<XrFrameEndInfo> endDesc;
						CHECK_RESULT(XR_SUCCESS, xrEndFrame(session, &endDesc));
					}
				}
			}
		}
	}
#endif // XR_USE_GRAPHICS_API_OPENGL
}
