// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Tests for function xrEnumerateReferenceSpaces
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#include <util/SessionGenerator.h>
#include <util/SystemTestUtils.h>

using Catch::Matchers::VectorContains;

TEST_CASE("xrEnumerateReferenceSpaces")
{
	auto generator = sessionGenerator();
	while (generator.next()) {
		auto&& sessCreator = generator.get();
		DYNAMIC_SECTION("Using a session described as " << sessCreator.getSessionDescription())
		{
			auto sessEtAl = sessCreator.invoke(XR_FORM_FACTOR_HEAD_MOUNTED_DISPLAY);
			XRS_GET_PARTS_FROM_SESSION_ET_AL(sessEtAl);
			auto refSpaceTypes = CHECK_TWO_CALL(XrReferenceSpaceType, xrEnumerateReferenceSpaces, session);
			REQUIRE_FALSE(refSpaceTypes.empty());

			THEN("Each reference space type should be recognized")
			{
				for (auto refSpaceType : refSpaceTypes) {
					CHECK_THAT(refSpaceType,
					           In<XrReferenceSpaceType>({XR_REFERENCE_SPACE_TYPE_LOCAL, XR_REFERENCE_SPACE_TYPE_STAGE,
					                                     XR_REFERENCE_SPACE_TYPE_VIEW}));
				}
			}
			THEN("Local and view spaces are be provided")
			{
				CHECK_THAT(refSpaceTypes, VectorContains(XR_REFERENCE_SPACE_TYPE_LOCAL));
				CHECK_THAT(refSpaceTypes, VectorContains(XR_REFERENCE_SPACE_TYPE_VIEW));
			}
		}
	}
}
