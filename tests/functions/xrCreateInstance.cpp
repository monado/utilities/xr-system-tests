// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Tests for function xrCreateInstance
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#include <util/StringVec.h>
#include <util/SystemTestUtils.h>
#include <util/XrPlatformInclude.h>
#include <xrtraits/TwoCall.h>
using xrtraits::doTwoCallInPlace;
using xrtraits::Initialized;

static const auto appInfo = XrApplicationInfo{"System Tests", 1, // app version
                                              "", 0,             // engine version
                                              XR_CURRENT_API_VERSION};

#ifdef XR_USE_PLATFORM_ANDROID
#define NEED_PLATFORM_STRUCT
static const Initialized<XrInstanceCreateInfoAndroidKHR> next;
#endif

#ifdef NEED_PLATFORM_STRUCT
#define CHAINED_PLATFORM_EXT_STRUCT next
#define CHAINED_PLATFORM_EXT_STRUCT_COMMA next,
#else
#define CHAINED_PLATFORM_EXT_STRUCT
#define CHAINED_PLATFORM_EXT_STRUCT_COMMA
#endif

TEST_CASE("xrCreateInstance_success", "[success]")
{
	StringVec extensions;
	StringVec layers;
	SECTION("Simple init")
	{
		Initialized<XrInstanceCreateInfo> instanceCreateInfo{
		    CHAINED_PLATFORM_EXT_STRUCT_COMMA XrInstanceCreateFlags(0),
		    appInfo,
		    layers.size(),
		    layers.data(),
		    extensions.size(),
		    extensions.data(),
		};
		INFO("This asserts that there are no required platform-specific structures for this platform.");
		ScopedInstance instance;
		REQUIRE_RESULT(XR_SUCCESS, xrCreateInstance(&instanceCreateInfo, instance.resetAndGetAddress()));
		REQUIRE(instance != XR_NULL_HANDLE_CPP);
	}

	SECTION("Ignore unexpected structures")
	{
		Initialized<XrActionCreateInfo> unexpected{CHAINED_PLATFORM_EXT_STRUCT};
		Initialized<XrInstanceCreateInfo> instanceCreateInfo{
		    unexpected,    XrInstanceCreateFlags(0), appInfo,           layers.size(),
		    layers.data(), extensions.size(),        extensions.data(),
		};
		ScopedInstance instance;
		REQUIRE_RESULT(XR_SUCCESS, xrCreateInstance(&instanceCreateInfo, instance.resetAndGetAddress()));
		REQUIRE(instance != XR_NULL_HANDLE_CPP);
	}
}

TEST_CASE("xrCreateInstance_error", "[error]")
{

	/// @todo handle mandatory platform-specific extensions
	StringVec extensions;
	StringVec layers;
	SECTION("Bogus extension name")
	{
		for (auto name : {"invalid_extension_name", ""}) {
			INFO("Invalid name testing: '" << name << "'");

			extensions.push_back(name);
			Initialized<XrInstanceCreateInfo> instanceCreateInfo{
			    CHAINED_PLATFORM_EXT_STRUCT_COMMA XrInstanceCreateFlags(0),
			    appInfo,
			    layers.size(),
			    layers.data(),
			    extensions.size(),
			    extensions.data(),
			};
			ScopedInstance instance;
			REQUIRE_RESULT(XR_ERROR_EXTENSION_NOT_PRESENT,
			               xrCreateInstance(&instanceCreateInfo, instance.resetAndGetAddress()));
			REQUIRE(instance == XR_NULL_HANDLE_CPP);
		}
	}
	SECTION("Bogus layer name")
	{
		for (auto name : {"invalid_layer_name", ""}) {
			INFO("Invalid name testing: '" << name << "'");

			layers.push_back(name);
			Initialized<XrInstanceCreateInfo> instanceCreateInfo{
			    CHAINED_PLATFORM_EXT_STRUCT_COMMA XrInstanceCreateFlags(0),
			    appInfo,
			    layers.size(),
			    layers.data(),
			    extensions.size(),
			    extensions.data(),
			};
			ScopedInstance instance;
			REQUIRE_RESULT(XR_ERROR_API_LAYER_NOT_PRESENT,
			               xrCreateInstance(&instanceCreateInfo, instance.resetAndGetAddress()));
			REQUIRE(instance == XR_NULL_HANDLE_CPP);
		}
	}

	/// @todo what if both are bogus? Return one of the not present errors? Return generic validation failure?

	SECTION("Platform-specific extension")
	{
#ifndef XR_USE_PLATFORM_ANDROID
		// must hard-code the name since the macro isn't defined without this platform define.
		for (auto type : {XR_TYPE_INSTANCE_CREATE_INFO_ANDROID_KHR}) {
			INFO("Wrong platform-specific extension type testing: '" << type << "'");
			XrBaseInStructure dummy{type, CHAINED_PLATFORM_EXT_STRUCT};
			Initialized<XrInstanceCreateInfo> instanceCreateInfo{
			    XrInstanceCreateFlags(0), appInfo, layers.size(), layers.data(), extensions.size(), extensions.data(),
			};
			// this awkward construction is because XrBaseInStructure doesn't work as a chain argument to Initialized
			instanceCreateInfo.get()->next = &dummy;
			ScopedInstance instance;
			REQUIRE_RESULT(XR_ERROR_INITIALIZATION_FAILED,
			               xrCreateInstance(&instanceCreateInfo, instance.resetAndGetAddress()));
			REQUIRE(instance == XR_NULL_HANDLE_CPP);
		}
#endif // !XR_USE_PLATFORM_ANDROID
	}
}
