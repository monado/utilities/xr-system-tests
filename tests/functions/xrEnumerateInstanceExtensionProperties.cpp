// Copyright 2018-2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  System tests related to Extensions
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#include <util/Config.h>

// Set up preprocessor defines to actually enable functionality.
#ifdef BUILD_OPENGL
#define XR_USE_GRAPHICS_API_OPENGL
#endif // BUILD_OPENGL

#ifdef BUILD_WITH_XLIB
#define XR_USE_PLATFORM_XLIB
#endif // BUILD_WITH_XLIB

#include <util/XrPlatformInclude.h>

#include <util/SystemTestUtils.h>

#include <string>

TEST_CASE("xrEnumerateInstanceExtensionProperties")
{
	WHEN("Enumerating instance extensions")
	{
		auto properties = REQUIRE_TWO_CALL(XrExtensionProperties, xrEnumerateInstanceExtensionProperties, nullptr);

		REQUIRE(properties.size() > 0);

		THEN("Debug utils extension should be listed")
		{
			CHECK_THAT(properties, VectorContainsPredicate<XrExtensionProperties>(
			                           [](XrExtensionProperties const& prop) {
				                           return std::string(XR_EXT_DEBUG_UTILS_EXTENSION_NAME) == prop.extensionName;
			                           },
			                           "XR_EXT_DEBUG_UTILS_EXTENSION_NAME == prop.extensionName"));
		}

		THEN("Headless extension should be listed")
		{
			CHECK_THAT(properties, VectorContainsPredicate<XrExtensionProperties>(
			                           [](XrExtensionProperties const& prop) {
				                           return std::string(XR_MND_HEADLESS_EXTENSION_NAME) == prop.extensionName;
			                           },
			                           "XR_MND_HEADLESS_EXTENSION_NAME == prop.extensionName"));
		}

#ifdef XR_USE_GRAPHICS_API_OPENGL

		THEN("OpenGL extension should be listed")
		{
			CHECK_THAT(properties, VectorContainsPredicate<XrExtensionProperties>(
			                           [](XrExtensionProperties const& prop) {
				                           return std::string(XR_KHR_OPENGL_ENABLE_EXTENSION_NAME) ==
				                                  prop.extensionName;
			                           },
			                           "XR_KHR_OPENGL_ENABLE_EXTENSION_NAME == prop.extensionName"));
		}
#endif // XR_USE_GRAPHICS_API_OPENGL
	}
}
