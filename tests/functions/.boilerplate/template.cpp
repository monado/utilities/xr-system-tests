{%- extends 'base.cpp' -%}
{%- macro fn() -%}{{stem}}.cpp{%- endmacro -%}
{%- block brief %}Tests for function {{stem}}{% endblock -%}
{%- block content %}
#include <util/SystemTestUtils.h>

#if 1

// If you have some tests where a call should succeed and others where a call should fail,
// use this first set of tests

TEST_CASE("{{stem}}_success", "[success]") {
    // Successful calls should go here
    auto scopedInstance = makeInstance({}, { REQUIRED_EXTENSIONS_HERE });
    XrInstance instance = scopedInstance.get();
}

TEST_CASE("{{stem}}_error", "[error]") {
    // Calls expected to error should go here
    auto scopedInstance = makeInstance({}, { REQUIRED_EXTENSIONS_HERE });
    XrInstance instance = scopedInstance.get();
}

#else

// If you don't have any "expected to return error code" tests,
// just use this.
TEST_CASE("{{stem}}") {
    auto scopedInstance = makeInstance({}, { REQUIRED_EXTENSIONS_HERE });
    XrInstance instance = scopedInstance.get();
}
#endif

{% endblock %}
