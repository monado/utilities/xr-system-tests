#!/bin/sh
# Copyright (c) 2019 Collabora, Ltd.
#
# SPDX-License-Identifier: BSL-1.0

set -e
(
    cd "$(dirname $0)"
    genfile $1 --type cpp
    (
        echo
        echo "# Uncomment and use only one of the following two lines, as required" 
        echo "# xrst_add_function_test($1 success error)" 
        echo "# xrst_add_function_test($1)" 
    )>> ../CMakeLists.txt
)
