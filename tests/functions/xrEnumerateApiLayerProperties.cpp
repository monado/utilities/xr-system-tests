// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Tests for function xrEnumerateApiLayerProperties
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#include <util/SystemTestUtils.h>

using Catch::Matchers::VectorContains;

TEST_CASE("xrEnumerateApiLayerProperties")
{
	WHEN("Enumerating API layers")
	{
		auto properties = REQUIRE_TWO_CALL(XrApiLayerProperties, xrEnumerateApiLayerProperties);
		THEN("No properties should have an empty or unterminated layerName")
		{
			for (const auto& prop : properties) {
				REQUIRE_FALSE(0 == int(prop.layerName[0]));
				REQUIRE_THAT(prop.layerName, NullTerminated<XR_MAX_API_LAYER_NAME_SIZE>());
			}
		}
		THEN("No properties should have an unterminated description")
		{
			//! @todo is this implied by the spec?
			for (const auto& prop : properties) {
				REQUIRE_THAT(prop.description, NullTerminated<XR_MAX_API_LAYER_DESCRIPTION_SIZE>());
			}
		}
	}
}
