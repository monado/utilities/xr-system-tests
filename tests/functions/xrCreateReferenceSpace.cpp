// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Tests for function xrCreateReferenceSpace
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#include <util/SessionGenerator.h>
#include <util/SystemTestUtils.h>

TEST_CASE("xrCreateReferenceSpace_success", "[success]")
{
	// Successful calls should go here
	auto generator = sessionGenerator();
	while (generator.next()) {
		auto&& sessCreator = generator.get();
		DYNAMIC_SECTION("Using a session described as " << sessCreator.getSessionDescription())
		{
			auto sessEtAl = sessCreator.invoke(XR_FORM_FACTOR_HEAD_MOUNTED_DISPLAY);
			XRS_GET_PARTS_FROM_SESSION_ET_AL(sessEtAl);
			// Get all supported reference space types and exercise them.
			auto refSpaceTypes = CHECK_TWO_CALL(XrReferenceSpaceType, xrEnumerateReferenceSpaces, session);

			for (auto refSpaceType : refSpaceTypes) {
				INFO("Reference space type is " << refSpaceType);
				XrSpace localSpace = XR_NULL_HANDLE_CPP;
				Initialized<XrReferenceSpaceCreateInfo> reference_space_create_info;
				reference_space_create_info.referenceSpaceType = refSpaceType;
				reference_space_create_info.poseInReferenceSpace.orientation = {0, 0, 0, 1};
				CHECK_RESULT(XR_SUCCESS, xrCreateReferenceSpace(session, &reference_space_create_info, &localSpace));
				CHECK_FALSE(localSpace == XR_NULL_HANDLE_CPP);
				CHECK_RESULT(XR_SUCCESS, xrDestroySpace(localSpace));
			}
		}
	}
}

TEST_CASE("xrCreateReferenceSpace_error", "[error]")
{
	// Calls expected to error should go here
	auto generator = sessionGenerator();
	while (generator.next()) {
		auto&& sessCreator = generator.get();
		DYNAMIC_SECTION("Using a session described as " << sessCreator.getSessionDescription())
		{
			auto sessEtAl = sessCreator.invoke(XR_FORM_FACTOR_HEAD_MOUNTED_DISPLAY);
			XRS_GET_PARTS_FROM_SESSION_ET_AL(sessEtAl);
			AND_WHEN("Calling CreateReferenceSpace with nonexistant reference space type")
			{
				XrSpace localSpace = XR_NULL_HANDLE_CPP;
				Initialized<XrReferenceSpaceCreateInfo> reference_space_create_info;
				reference_space_create_info.referenceSpaceType = XR_REFERENCE_SPACE_TYPE_MAX_ENUM;
				reference_space_create_info.poseInReferenceSpace.orientation = {0, 0, 0, 1};
				REQUIRE_RESULT(XR_ERROR_REFERENCE_SPACE_UNSUPPORTED,
				               xrCreateReferenceSpace(session, &reference_space_create_info, &localSpace));
				REQUIRE(localSpace == XR_NULL_HANDLE_CPP);
			}

			// Get all supported reference space types and exercise them.
			auto refSpaceTypes = CHECK_TWO_CALL(XrReferenceSpaceType, xrEnumerateReferenceSpaces, session);

			for (auto refSpaceType : refSpaceTypes) {
				DYNAMIC_SECTION("Reference space type: " << refSpaceType)
				{
					AND_WHEN("Calling CreateReferenceSpace with non-unit quat")
					{
						XrSpace localSpace = XR_NULL_HANDLE_CPP;
						Initialized<XrReferenceSpaceCreateInfo> reference_space_create_info;
						reference_space_create_info.referenceSpaceType = refSpaceType;
						reference_space_create_info.poseInReferenceSpace.orientation = {1, 1, 1, 1};
						REQUIRE_RESULT(XR_ERROR_POSE_INVALID,
						               xrCreateReferenceSpace(session, &reference_space_create_info, &localSpace));
						REQUIRE(localSpace == XR_NULL_HANDLE_CPP);
					}
					AND_WHEN("Calling CreateReferenceSpace with zero quat")
					{
						XrSpace localSpace = XR_NULL_HANDLE_CPP;
						Initialized<XrReferenceSpaceCreateInfo> reference_space_create_info;
						reference_space_create_info.referenceSpaceType = refSpaceType;
						// zero-init quaternion is invalid
						REQUIRE_RESULT(XR_ERROR_POSE_INVALID,
						               xrCreateReferenceSpace(session, &reference_space_create_info, &localSpace));
						REQUIRE(localSpace == XR_NULL_HANDLE_CPP);
					}
				}
			}
		}
	}
}
