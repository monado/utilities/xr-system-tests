// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Test for function xrGetInstanceProcAddr
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#include <util/Instance.h>
#include <util/SystemTestUtils.h>

TEST_CASE("xrGetInstanceProcAddr_success", "[success]")
{
	WHEN("providing a null instance")
	{
		PFN_xrVoidFunction pfn = reinterpret_cast<PFN_xrVoidFunction>(0xBADF00D);
		THEN("Request for xrEnumerateInstanceExtensionProperties should succeed")
		{
			REQUIRE_RESULT(XR_SUCCESS,
			               xrGetInstanceProcAddr(XR_NULL_HANDLE_CPP, "xrEnumerateInstanceExtensionProperties", &pfn));
			REQUIRE(pfn != nullptr);
		}
		THEN("Request for xrEnumerateApiLayerProperties should succeed")
		{
			REQUIRE_RESULT(XR_SUCCESS,
			               xrGetInstanceProcAddr(XR_NULL_HANDLE_CPP, "xrEnumerateApiLayerProperties", &pfn));
			REQUIRE(pfn != nullptr);
		}
		THEN("Request for xrCreateInstance should succeed")
		{
			REQUIRE_RESULT(XR_SUCCESS, xrGetInstanceProcAddr(XR_NULL_HANDLE_CPP, "xrCreateInstance", &pfn));
			REQUIRE(pfn != nullptr);
		}
	}
	WHEN("providing a valid instance")
	{
		auto inst = makeInstance({}, {});
		XrInstance instance = inst->getInstance();

		PFN_xrVoidFunction pfn = reinterpret_cast<PFN_xrVoidFunction>(0xBADF00D);
		THEN("Request for xrEnumerateInstanceExtensionProperties should succeed")
		{
			REQUIRE_RESULT(XR_SUCCESS, xrGetInstanceProcAddr(instance, "xrEnumerateInstanceExtensionProperties", &pfn));
			REQUIRE(pfn != nullptr);
		}
		THEN("Request for xrEnumerateApiLayerProperties should succeed")
		{
			REQUIRE_RESULT(XR_SUCCESS, xrGetInstanceProcAddr(instance, "xrEnumerateApiLayerProperties", &pfn));
			REQUIRE(pfn != nullptr);
		}
		THEN("Request for xrCreateInstance should succeed")
		{
			REQUIRE_RESULT(XR_SUCCESS, xrGetInstanceProcAddr(instance, "xrCreateInstance", &pfn));
			REQUIRE(pfn != nullptr);
		}
		THEN("Request for xrDestroyInstance should succeed")
		{
			REQUIRE_RESULT(XR_SUCCESS, xrGetInstanceProcAddr(instance, "xrDestroyInstance", &pfn));
			REQUIRE(pfn != nullptr);
		}
	}
	// Not required to detect if instance is non-null but invalid?
}

TEST_CASE("xrGetInstanceProcAddr_error", "[error]")
{
	WHEN("providing a null instance")
	{
		PFN_xrVoidFunction pfn = reinterpret_cast<PFN_xrVoidFunction>(0xBADF00D);
		THEN("Request for xrDestroyInstance should fail")
		{
			REQUIRE_RESULT(XR_ERROR_HANDLE_INVALID,
			               xrGetInstanceProcAddr(XR_NULL_HANDLE_CPP, "xrDestroyInstance", &pfn));
			REQUIRE(pfn == nullptr);
		}
		THEN("Request for FakeFunction should fail")
		{
			REQUIRE_THAT(xrGetInstanceProcAddr(XR_NULL_HANDLE_CPP, "FakeFunction", &pfn),
			             In<XrResultCode>({XR_ERROR_HANDLE_INVALID, XR_ERROR_FUNCTION_UNSUPPORTED}));
			REQUIRE(pfn == nullptr);
		}
	}
	/// @todo what if instance is non-null but invalid?
	WHEN("providing a valid instance")
	{
		//! @todo check an invalid name: should be XR_ERROR_FUNCTION_UNSUPPORTED, or maybe
		//! XR_ERROR_HANDLE_INVALID if XR_NULL_HANDLE_CPP used.
		auto inst = makeInstance({}, {});
		XrInstance instance = inst->getInstance();

		PFN_xrVoidFunction pfn = reinterpret_cast<PFN_xrVoidFunction>(0xBADF00D);
		THEN("Request for FakeFunction should fail")
		{
			REQUIRE_RESULT(XR_ERROR_FUNCTION_UNSUPPORTED, xrGetInstanceProcAddr(instance, "FakeFunction", &pfn));
			REQUIRE(pfn == nullptr);
		}
	}
}
