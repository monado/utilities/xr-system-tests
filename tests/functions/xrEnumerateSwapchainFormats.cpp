// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Tests for function xrEnumerateSwapchainFormats
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#include <util/SessionGenerator.h>
#include <util/SystemTestUtils.h>

TEST_CASE("xrEnumerateSwapchainFormats")
{
	WHEN("Using graphical session types")
	{
		auto generator = graphicalSessionGenerator();
		while (generator.next()) {
			auto&& sessCreator = generator.get();
			DYNAMIC_SECTION("Using a session described as " << sessCreator.getSessionDescription())
			{
				auto sessEtAl = sessCreator.invoke(XR_FORM_FACTOR_HEAD_MOUNTED_DISPLAY);
				XRS_GET_PARTS_FROM_SESSION_ET_AL(sessEtAl);

				auto formats = REQUIRE_TWO_CALL(int64_t, xrEnumerateSwapchainFormats, session);
				REQUIRE(formats.size() > 0);

				/// @todo switch on sessEtAl->getApi() to perform per-api checks.
			}
		}
	}
	WHEN("Using headless session")
	{
		auto generator = headlessSessionGenerator();
		while (generator.next()) {
			auto&& sessCreator = generator.get();
			DYNAMIC_SECTION("Using a session described as " << sessCreator.getSessionDescription())
			{
				auto sessEtAl = sessCreator.invoke(XR_FORM_FACTOR_HEAD_MOUNTED_DISPLAY);
				XRS_GET_PARTS_FROM_SESSION_ET_AL(sessEtAl);

				auto formats = REQUIRE_TWO_CALL(int64_t, xrEnumerateSwapchainFormats, session);
				REQUIRE(formats.empty());
			}
		}
	}
}
