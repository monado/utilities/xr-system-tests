// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Tests for function xrBeginSession
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#include <util/SessionGenerator.h>
#include <util/SystemTestUtils.h>

TEST_CASE("xrBeginSession_success", "[success]")
{
	// Successful calls should go here
	auto generator = sessionGenerator();
	while (generator.next()) {
		auto&& sessCreator = generator.get();
		DYNAMIC_SECTION("Using a session described as " << sessCreator.getSessionDescription())
		{
			auto sessEtAl = sessCreator.invoke(XR_FORM_FACTOR_HEAD_MOUNTED_DISPLAY);
			XRS_GET_PARTS_FROM_SESSION_ET_AL(sessEtAl);
			REQUIRE(instance != XR_NULL_HANDLE_CPP);
			REQUIRE(session != XR_NULL_HANDLE_CPP);
			REQUIRE(systemId != XR_NULL_SYSTEM_ID);
		}
	}
}

TEST_CASE("xrBeginSession_error", "[error]")
{
	// Calls expected to error should go here
}
