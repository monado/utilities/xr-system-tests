// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Tests for function xrCreateSwapchain
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#include <util/BitmaskGenerator.h>
#include <util/SessionGenerator.h>
#include <util/SystemTestUtils.h>

static inline void checkCreateSwapchain(XrSession session, int64_t format, uint32_t width, uint32_t height)
{
	std::initializer_list<BitmaskData> attachmentBits = {
	    {"XR_SWAPCHAIN_USAGE_COLOR_ATTACHMENT_BIT", 0x00000001},
	    {"XR_SWAPCHAIN_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT", 0x00000002}};
	for (auto attachment : attachmentBits) {
		auto&& generator = bitmaskGeneratorIncluding0({
		    {"XR_SWAPCHAIN_USAGE_UNORDERED_ACCESS_BIT", 0x00000004},
		    {"XR_SWAPCHAIN_USAGE_TRANSFER_SRC_BIT", 0x00000008},
		    {"XR_SWAPCHAIN_USAGE_TRANSFER_DST_BIT", 0x00000010},
		    {"XR_SWAPCHAIN_USAGE_SAMPLED_BIT", 0x00000020},
		    {"XR_SWAPCHAIN_USAGE_MUTABLE_FORMAT_BIT", 0x00000040},
		});
		while (generator.next()) {
			auto fullBits = attachment | generator.get();
			DYNAMIC_SECTION("swapchain create flags described as " << fullBits.description)
			{
				Initialized<XrSwapchainCreateInfo> createInfo{
				    XrSwapchainCreateFlags(0), // create flags
				    fullBits.bitmask,          // usage flags,
				    format,
				    uint32_t(1), // sampleCount
				    width,
				    height,
				    uint32_t(1), // face count
				    uint32_t(1), // array size
				    uint32_t(1), // mip count
				};
				XrSwapchain swapchain = XR_NULL_HANDLE_CPP;
				REQUIRE_RESULT(XR_SUCCESS, xrCreateSwapchain(session, &createInfo, &swapchain));
				REQUIRE_FALSE(swapchain == XR_NULL_HANDLE_CPP);
				CHECK_RESULT(XR_SUCCESS, xrDestroySwapchain(swapchain));
			}
		}
	}
}

TEST_CASE("xrCreateSwapchain_success", "[success]")
{
	// Successful calls should go here
	auto generator = graphicalSessionGenerator();
	while (generator.next()) {
		auto&& sessCreator = generator.get();
		DYNAMIC_SECTION("Using a session described as " << sessCreator.getSessionDescription())
		{
			auto sessEtAl = sessCreator.invoke(XR_FORM_FACTOR_HEAD_MOUNTED_DISPLAY);
			XRS_GET_PARTS_FROM_SESSION_ET_AL(sessEtAl);
			// Initialized<XrSystemProperties> system_properties;
			// CHECK_RESULT(XR_SUCCESS, xrGetSystemProperties(instance, systemId, &system_properties));

			auto formats = REQUIRE_TWO_CALL(int64_t, xrEnumerateSwapchainFormats, session);
			REQUIRE_FALSE(formats.empty());

			auto viewConfigTypes =
			    REQUIRE_TWO_CALL(XrViewConfigurationType, xrEnumerateViewConfigurations, instance, systemId);
			for (auto viewConfig : viewConfigTypes) {
				DYNAMIC_SECTION("Using enumerated view config " << viewConfig)
				{
					auto viewConfigViews = REQUIRE_TWO_CALL(XrViewConfigurationView, xrEnumerateViewConfigurationViews,
					                                        instance, systemId, viewConfig);

					for (auto format : formats) {
						DYNAMIC_SECTION("using enumerated swapchain format " << format)
						{
							checkCreateSwapchain(session, format, viewConfigViews.front().recommendedImageRectWidth,
							                     viewConfigViews.front().recommendedImageRectHeight);
						}
					}
				}
			}
		}
	}
}

TEST_CASE("xrCreateSwapchain_error", "[error]")
{
	// Calls expected to error should go here
	auto generator = sessionGenerator();
	while (generator.next()) {
		auto&& sessCreator = generator.get();
		DYNAMIC_SECTION("Using a session described as " << sessCreator.getSessionDescription())
		{
			auto sessEtAl = sessCreator.invoke(XR_FORM_FACTOR_HEAD_MOUNTED_DISPLAY);
			XRS_GET_PARTS_FROM_SESSION_ET_AL(sessEtAl);
		}
	}
}
