# Copyright 2018-2019 Collabora, Ltd.
# Distributed under the Boost Software License, Version 1.0.
# (See accompanying file LICENSE_1_0.txt or copy at
# http://www.boost.org/LICENSE_1_0.txt)
#
# Original Author:
# 2019 Ryan Pavlik <ryan.pavlik@collabora.com>

include(CheckCXXCompilerFlag)
function(target_compile_options_conditional TARGET SCOPE)
    foreach(flag ${ARGN})
        string(REGEX REPLACE "[^A-Za-z0-9]+" "_" sanitized "${flag}")
        check_cxx_compiler_flag(${flag} COMPILER_SUPPORTS_${var})
        if(COMPILER_SUPPORTS_${var})
            target_compile_options(${TARGET} ${SCOPE} "${flag}")
        endif()
    endforeach()
endfunction()
