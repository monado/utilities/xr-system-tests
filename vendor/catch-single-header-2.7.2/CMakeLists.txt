
# Copyright 2019, Collabora, Ltd.
#
# SPDX-License-Identifier: BSL-1.0

# Create an interface target to consume Catch
add_library(xrtests-catch-interface INTERFACE)
target_include_directories(xrtests-catch-interface SYSTEM INTERFACE "${CMAKE_CURRENT_SOURCE_DIR}")
include(Catch.cmake)
