# Copyright (c) 2018-2019 Collabora, Ltd.
#
# SPDX-License-Identifier: BSL-1.0

find_package(Threads)
# Create an interface target to consume Catch
if(CATCH2_FOUND)
    add_library(xrtests-catch-interface INTERFACE)
    target_link_libraries(xrtests-catch-interface
        INTERFACE
        ${CMAKE_THREAD_LIBS_INIT})
    target_link_libraries(xrtests-catch-interface INTERFACE Catch2::Catch2)
else()
    add_subdirectory(catch-single-header-2.7.2)
endif()


if(BUILD_OPENGL)
    add_subdirectory(glad)
endif()

if(NOT XRTRAITS_FOUND)
    message(STATUS "Using vendored xrtraits")
    set(BUILD_WITH_EXCEPTIONS OFF CACHE INTERNAL "" FORCE)
    set(BUILD_WITH_CXX14 ON CACHE INTERNAL "" FORCE)
    set(BUILD_WITH_CXX17 OFF CACHE INTERNAL "" FORCE)
    add_subdirectory(xrtraits)
endif()
