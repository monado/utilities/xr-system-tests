// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header for "x-macro" usage - when you want to do something for each handle type.
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#ifndef XR_NULL_HANDLE
#error "Must include openxr.h before including this header."
#endif

#ifndef XRUTIL_HANDLE_TYPE
#error "Must define an XRUTIL_HANDLE_TYPE function-style macro before including ForEachHandleType.h"
#endif

#ifdef XRUTIL_HANDLE_SKIP_INSTANCE
#undef XRUTIL_HANDLE_SKIP_INSTANCE
#else
XRUTIL_HANDLE_TYPE(Instance);
#endif

#ifdef XRUTIL_HANDLE_SKIP_SESSION
#undef XRUTIL_HANDLE_SKIP_SESSION
#else
XRUTIL_HANDLE_TYPE(Session);
#endif

#ifdef XRUTIL_HANDLE_SKIP_SWAPCHAIN
#undef XRUTIL_HANDLE_SKIP_SWAPCHAIN
#else
XRUTIL_HANDLE_TYPE(Swapchain);
#endif

#ifdef XRUTIL_HANDLE_SKIP_ACTION
#undef XRUTIL_HANDLE_SKIP_ACTION
#else
XRUTIL_HANDLE_TYPE(Action);
#endif

#ifdef XRUTIL_HANDLE_SKIP_ACTIONSET
#undef XRUTIL_HANDLE_SKIP_ACTIONSET
#else
XRUTIL_HANDLE_TYPE(ActionSet);
#endif

#ifdef XRUTIL_HANDLE_SKIP_SPACE
#undef XRUTIL_HANDLE_SKIP_SPACE
#else
XRUTIL_HANDLE_TYPE(Space);
#endif

#ifdef XRUTIL_HANDLE_SKIP_DEBUGUTILSMESSENGEREXT
#undef XRUTIL_HANDLE_SKIP_DEBUGUTILSMESSENGEREXT
#elif defined(XR_EXT_debug_utils)
XRUTIL_HANDLE_TYPE(DebugUtilsMessengerEXT);
#endif

#undef XRUTIL_HANDLE_TYPE
