// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Implementation
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#include "Headless.h"
#include "System.h"

// use this instead of letting glfw include opengl,
// to avoid xlib destroying things with macros.
#include "XrPlatformInclude.h"

#if defined(XR_USE_PLATFORM_WIN32)
#define GLFW_EXPOSE_NATIVE_WIN32
#define GLFW_EXPOSE_NATIVE_WGL
#elif defined(XR_USE_PLATFORM_XLIB)
#define GLFW_EXPOSE_NATIVE_GLX
#define GLFW_EXPOSE_NATIVE_X11
#endif

#include <GLFW/glfw3.h>
#include <GLFW/glfw3native.h>

#include "System.h"
#include "SystemTestUtils.h"

#include "OpenGL.h"

#include <xrtraits/InitXrType.h>

using xrtraits::Initialized;

class HeadlessSession : public SessionInterface
{
private:
	XrSystemId systemId_;
	ScopedSession session_;

public:
	~HeadlessSession() override {}

	XrSession get() const override { return session_.get(); }
	XrSystemId getSystemId() const override { return systemId_; }

	HeadlessSession(XrInstance instance, XrSystemId systemId) : systemId_(systemId)
	{
		assert(systemId != XR_NULL_SYSTEM_ID);
		XrSession session = XR_NULL_HANDLE_CPP;
		{
			XrSessionCreateInfo session_create_info{XR_TYPE_SESSION_CREATE_INFO, nullptr, XrSessionCreateFlags(0),
			                                        systemId};

			REQUIRE_RESULT(XR_SUCCESS, xrCreateSession(instance, &session_create_info, &session));
			REQUIRE_FALSE(XR_NULL_HANDLE_CPP == session);
		}
		session_.reset(session);
	}
};

SessionInterfacePtr makeHeadlessSession(XrInstance instance, XrSystemId systemId)
{
	CAPTURE(systemId);
	if (systemId == XR_NULL_SYSTEM_ID) {
		return nullptr;
	}
	return std::make_unique<HeadlessSession>(instance, systemId);
}

SessionInterfacePtr makeHeadlessSessionWithFormFactor(XrInstance instance, XrFormFactor formFactor)
{
	return makeHeadlessSession(instance, formFactorToSystemId(instance, formFactor));
}
