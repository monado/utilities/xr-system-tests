// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

#include <openxr/openxr.h>

#include <cassert>
#include <initializer_list>
#include <iosfwd>
#include <vector>

#include <xrtraits/UniqueHandles.h>

/// Dummy type used to provide a unique identity for XR_NULL_HANDLE, for comparisons, etc.
/// Implicitly convertible to XR_NULL_HANDLE in all the places you want.
///
/// Typically just use the instance XR_NULL_HANDLE_CPP
struct NullHandleType
{
#if XR_PTR_SIZE == 8

#define MAKE_CONVERSION_FUNCTION(T)                                                                                    \
	operator T() const { return XR_NULL_HANDLE; }
	MAKE_CONVERSION_FUNCTION(XrInstance)
	MAKE_CONVERSION_FUNCTION(XrSession)
	MAKE_CONVERSION_FUNCTION(XrSpace)
	MAKE_CONVERSION_FUNCTION(XrAction)
	MAKE_CONVERSION_FUNCTION(XrSwapchain)
	MAKE_CONVERSION_FUNCTION(XrActionSet)
	MAKE_CONVERSION_FUNCTION(XrDebugUtilsMessengerEXT)
#else
	// 32-bit, just a uint64_t
	operator uint64_t() const { return XR_NULL_HANDLE; }
#endif
};
constexpr NullHandleType XR_NULL_HANDLE_CPP{};

std::ostream& operator<<(std::ostream& os, NullHandleType const&);

template <typename HandleType, typename Destroyer> class ScopedHandle;
/// Used by ScopedHandle to allow it to be set "directly" by functions taking a pointer to a handle.
template <typename HandleType, typename Destroyer> class ScopedHandleResetProxy
{
public:
	explicit ScopedHandleResetProxy(ScopedHandle<HandleType, Destroyer>& parent) : parent_(parent), active_(true) {}
	~ScopedHandleResetProxy();

	ScopedHandleResetProxy(ScopedHandleResetProxy const&) = delete;
	ScopedHandleResetProxy& operator=(ScopedHandleResetProxy const&) = delete;
	ScopedHandleResetProxy& operator=(ScopedHandleResetProxy&&) = delete;
	ScopedHandleResetProxy(ScopedHandleResetProxy&& other) : parent_(other.parent_)
	{
		std::swap(active_, other.active_);
		std::swap(addressGot_, other.addressGot_);
		std::swap(handle_, other.handle_);
	}

	operator HandleType*()
	{
		assert(!addressGot_);
		addressGot_ = true;
		return &handle_;
	}

private:
	ScopedHandle<HandleType, Destroyer>& parent_;
	bool active_ = false;
	bool addressGot_ = false;
	HandleType handle_ = XR_NULL_HANDLE;
};

template <typename HandleType, typename Destroyer> class ScopedHandle
{
public:
	/// Default (empty) constructor
	ScopedHandle() = default;

	/// Empty constructor when we need a destroyer instance.
	ScopedHandle(Destroyer d) : h_(XR_NULL_HANDLE), d_(d) {}

	/// Explicit constructor from handle
	explicit ScopedHandle(HandleType h) : h_(h) {}
	/// Constructor from handle when we need a destroyer instance.
	ScopedHandle(HandleType h, Destroyer d) : h_(h), d_(d) {}

	/// Destructor
	~ScopedHandle() { reset(); }

	/// Non-copyable
	ScopedHandle(ScopedHandle const&) = delete;
	/// Non-copy-assignable
	ScopedHandle& operator=(ScopedHandle const&) = delete;
	/// Move-constructible
	ScopedHandle(ScopedHandle&& other) { std::swap(h_, other.h_); }

	/// Move-assignable
	ScopedHandle& operator=(ScopedHandle&& other)
	{
		std::swap(h_, other.h_);
		other.reset();
		return *this;
	}

	/// Is this handle valid?
	explicit operator bool() const { return h_ != XR_NULL_HANDLE; }

	/// Destroy the owned handle, if any.
	void reset()
	{
		if (h_ != XR_NULL_HANDLE_CPP) {
			d_(h_);
			h_ = XR_NULL_HANDLE_CPP;
		}
	}

	/// Assign a new handle into this object's control, destroying the old one if applicable.
	void reset(HandleType h)
	{
		reset();
		h_ = h;
	}

	/// Access the raw handle without affecting ownership or lifetime.
	HandleType get() const { return h_; }

	/// Release the handle from this object's control.
	HandleType release()
	{
		HandleType ret = h_;
		h_ = XR_NULL_HANDLE_CPP;
		return ret;
	}

	/// Call in a paramter that requires a pointer to a handle, to set it "directly" in here.
	ScopedHandleResetProxy<HandleType, Destroyer> resetAndGetAddress()
	{
		reset();
		return ScopedHandleResetProxy<HandleType, Destroyer>(*this);
	}

private:
	HandleType h_ = XR_NULL_HANDLE_CPP;
	Destroyer d_;
};

template <typename HandleType, typename Destroyer>
inline bool operator==(ScopedHandle<HandleType, Destroyer> const& handle, NullHandleType const&)
{
	return handle.get() == XR_NULL_HANDLE_CPP;
}
template <typename HandleType, typename Destroyer>
inline bool operator==(NullHandleType const&, ScopedHandle<HandleType, Destroyer> const& handle)
{
	return handle.get() == XR_NULL_HANDLE_CPP;
}

template <typename HandleType, typename Destroyer>
inline bool operator!=(ScopedHandle<HandleType, Destroyer> const& handle, NullHandleType const&)
{
	return handle.get() != XR_NULL_HANDLE_CPP;
}
template <typename HandleType, typename Destroyer>
inline bool operator!=(NullHandleType const&, ScopedHandle<HandleType, Destroyer> const& handle)
{
	return handle.get() != XR_NULL_HANDLE_CPP;
}

template <typename HandleType, typename Destroyer>
inline ScopedHandleResetProxy<HandleType, Destroyer>::~ScopedHandleResetProxy()
{
	if (active_) {
		assert(addressGot_ && "Called resetAndGetAddress() without passing the result to a pointer-taking function.");
		parent_.reset(handle_);
	}
}

namespace testing_handle_destroyers {
class DestroyDebugUtilsMessengerEXT
{
public:
	DestroyDebugUtilsMessengerEXT(XrInstance instance);
	void operator()(XrDebugUtilsMessengerEXT handle) const;

private:
	PFN_xrDestroyDebugUtilsMessengerEXT pfn_;
};

// Because we explicitly defined it above.
#define XRUTIL_HANDLE_SKIP_DEBUGUTILSMESSENGEREXT
#define XRUTIL_HANDLE_TYPE(HandleName)                                                                                 \
	class Destroy##HandleName                                                                                          \
	{                                                                                                                  \
	public:                                                                                                            \
		Destroy##HandleName() = default;                                                                               \
		void operator()(Xr##HandleName handle) const;                                                                  \
	};

#include "ForEachHandleType.h"

} // namespace testing_handle_destroyers

#define XRUTIL_HANDLE_TYPE(HandleName)                                                                                 \
	using Scoped##HandleName = ScopedHandle<Xr##HandleName, testing_handle_destroyers::Destroy##HandleName>

#include "ForEachHandleType.h"
