// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Implementation
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#include "Session.h"

SessionInterface::SessionInterface() = default;
SessionInterface::~SessionInterface() = default;
