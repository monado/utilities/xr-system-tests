// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Implementation
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#include "GraphicsSupport.h"

#include "Instance.h"
#include "XrPlatformInclude.h"

#include <xrtraits/TwoCall.h>

#include <stdexcept>

using xrtraits::doTwoCallInPlace;

GraphicsSupport::GraphicsSupport()
{
	std::vector<XrExtensionProperties> extensions;

	if (!XR_SUCCEEDED(doTwoCallInPlace(extensions, xrEnumerateInstanceExtensionProperties, nullptr))) {
		throw std::runtime_error("Could not enumerate extensions.");
	}

	mnd_headless = hasExtension(extensions, XR_MND_HEADLESS_EXTENSION_NAME);
	opengl = hasExtension(extensions, XR_KHR_OPENGL_ENABLE_EXTENSION_NAME);
#ifdef XR_USE_GRAPHICS_API_VULKAN
	vulkan = hasExtension(extensions, XR_KHR_);
#endif // XR_USE_GRAPHICS_API_VULKAN
}

GraphicsSupport const& GraphicsSupport::get()
{
	static GraphicsSupport instance;
	return instance;
}
