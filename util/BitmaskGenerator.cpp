// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Implementation
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#include "BitmaskGenerator.h"

#include <vector>
#include <memory>

namespace {

/*!
 * GeneratorBase implementation for the BitmaskGenerator - implementation details.
 *
 * @see bitmaskGenerator for the factory function to create one of these.
 *
 * Uses the binary of an integer index as a selection of
 * which supplied bitmasks should be enabled in a given generated output.
 * Yes, this is a bitmask that selects bitmasks.
 */
class BitmaskGenerator : public GeneratorBase<BitmaskData const&>
{
public:
	~BitmaskGenerator() override = default;
	static std::unique_ptr<GeneratorBase<BitmaskData const&>> create(bool zeroOk,
	                                                                 std::initializer_list<BitmaskData> const& bits)
	{
		return std::make_unique<BitmaskGenerator>(zeroOk, bits);
	}

	BitmaskGenerator(bool zeroOk, std::initializer_list<BitmaskData> const& bits) : bits_(bits), zeroOk_(zeroOk) {}

	BitmaskData const& get() override { return current_; }

	bool next() override
	{
		// Return the zeroth combination first.
		if (zeroOk_ && !gotZeroYet_) {
			gotZeroYet_ = true;
			return true;
		}
		// Otherwise, move on to the next index
		currentIndex_++;
		const auto n = bits_.size();
		if (currentIndex_ >= (uint64_t(0x1) << n)) {
			// n is highest bit number + 1.
			// Thus, the largest mask is 0x1 << n - 1.
			// If we exceed that, we've run out of combinations.
			return false;
		}

		BitmaskData accumulate{{}, 0};
		// Loop through the bits of our index to determine whether to enable a given bitmask
		for (size_t i = 0; i < n; ++i) {
			uint64_t indexBit = (uint64_t(0x1) << i);
			if ((indexBit & currentIndex_) != 0) {
				// Yes, enable this bitmask
				accumulate |= bits_[i];
			}
		}
		current_ = accumulate;
		return true;
	}

private:
	std::vector<BitmaskData> bits_;
	bool zeroOk_;
	bool gotZeroYet_ = false;
	uint64_t currentIndex_ = 0;
	BitmaskData current_;
};

} // namespace

BitmaskData operator|(BitmaskData const& lhs, BitmaskData const& rhs)
{
	if (lhs.empty()) {
		return rhs;
	}
	if (rhs.empty()) {
		return lhs;
	}
	// If we are here, we are combining two non-empty.
	BitmaskData ret{lhs};
	ret |= rhs;
	return ret;
}

BitmaskData& BitmaskData::operator|=(BitmaskData const& other)
{
	if (this == &other) {
		return *this;
	}
	if (other.empty()) {
		return *this;
	}
	if (empty()) {
		*this = other;
		return *this;
	}
	description += " | ";
	description += other.description;
	bitmask |= other.bitmask;
	return *this;
}

GeneratorWrapper<BitmaskData const&> bitmaskGeneratorIncluding0(std::initializer_list<BitmaskData> const& bits)
{
	return {BitmaskGenerator::create(true, bits)};
}

GeneratorWrapper<BitmaskData const&> bitmaskGenerator(std::initializer_list<BitmaskData> const& bits)
{
	return {BitmaskGenerator::create(false, bits)};
}
