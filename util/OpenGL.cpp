// Copyright 2018-2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Implementation
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 * @author Christoph Haag <christop.haag@collabora.com>
 */

#include "OpenGL.h"

#ifdef XR_USE_GRAPHICS_API_OPENGL

// use this instead of letting glfw include opengl,
// to avoid xlib destroying things with macros.
#include "XrPlatformInclude.h"

#if defined(XR_USE_PLATFORM_WIN32)
#define GLFW_EXPOSE_NATIVE_WIN32
#define GLFW_EXPOSE_NATIVE_WGL
#elif defined(XR_USE_PLATFORM_XLIB)
#define GLFW_EXPOSE_NATIVE_GLX
#define GLFW_EXPOSE_NATIVE_X11
#endif

#include <GLFW/glfw3.h>
#include <GLFW/glfw3native.h>

#include "System.h"
#include <util/SystemTestUtils.h>

#include <xrtraits/InitXrType.h>

using xrtraits::Initialized;

static void error_callback(int error, const char* description)
{
	CATCH_ERROR("Got OpenGL error callback: error " << error << ", description: " << description);
}
class OpenGLSession : public SessionInterface
{
private:
	XrSystemId systemId_;
	GLFWwindow* window_ = nullptr;
	ScopedSession session_;

public:
	~OpenGLSession() override {}

	XrSession get() const override { return session_.get(); }
	XrSystemId getSystemId() const override { return systemId_; }

	OpenGLSession(XrInstance instance, XrSystemId systemId) : systemId_(systemId)
	{
		assert(systemId != XR_NULL_SYSTEM_ID);
		if (!glfwInit()) {
			throw std::runtime_error("Could not init glfw");
		}

		// Must be called before creating session.
		Initialized<XrGraphicsRequirementsOpenGLKHR> graphicsRequirements;
		REQUIRE_RESULT(XR_SUCCESS, xrGetOpenGLGraphicsRequirementsKHR(instance, systemId, &graphicsRequirements));
		// Create a GL window

		glfwSetErrorCallback(error_callback);

		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

		window_ = glfwCreateWindow(640, 480, "Window Used for Tests", NULL, NULL);
		if (window_ == nullptr) {
			CATCH_ERROR("Couldn't create glfw window");
		}

		glfwHideWindow(window_);

		glfwMakeContextCurrent(window_);

		if (!gladLoadGL(glfwGetProcAddress)) {
			CATCH_ERROR("GLAD: Failed to load GL function pointers!");
		}
		XrSession session = XR_NULL_HANDLE_CPP;
		{
#ifdef XR_USE_PLATFORM_WIN32
			Initialized<XrGraphicsBindingOpenGLWin32KHR> graphicsBinding{nullptr /* todo wrong */,
			                                                             glfwGetWGLContext(window_)};
#elif defined(XR_USE_PLATFORM_XLIB)
			Initialized<XrGraphicsBindingOpenGLXlibKHR> graphicsBinding{glfwGetX11Display()};
			graphicsBinding.glxDrawable = glfwGetGLXWindow(window_);
			graphicsBinding.glxContext = glfwGetGLXContext(window_);
			/*! @todo populate rest */
#elif defined(XR_USE_PLATFORM_XCB)
#error "not implemented"
			// Initialized<XrGraphicsBindingOpenGLXcbKHR> graphicsBinding{window_.connection /*! @todo
			// populate rest */};
#elif defined(XR_USE_PLATFORM_WAYLAND)
#error "not implemented"
			// Initialized<XrGraphicsBindingOpenGLWaylandKHR> graphicsBinding{window_.display /*! @todo
			// populate rest */};
#endif

			XrSessionCreateInfo session_create_info{XR_TYPE_SESSION_CREATE_INFO, &graphicsBinding,
			                                        XrSessionCreateFlags(0), systemId};

			REQUIRE_RESULT(XR_SUCCESS, xrCreateSession(instance, &session_create_info, &session));
			REQUIRE_FALSE(XR_NULL_HANDLE_CPP == session);
		}
		session_.reset(session);
	}
};

SessionInterfacePtr makeOpenGLSession(XrInstance instance, XrSystemId systemId)
{
	if (systemId == XR_NULL_SYSTEM_ID) {
		return nullptr;
	}
	return std::make_unique<OpenGLSession>(instance, systemId);
}

SessionInterfacePtr makeOpenGLSessionWithFormFactor(XrInstance instance, XrFormFactor formFactor)
{
	return makeOpenGLSession(instance, formFactorToSystemId(instance, formFactor));
}

#endif // XR_USE_GRAPHICS_API_OPENGL
