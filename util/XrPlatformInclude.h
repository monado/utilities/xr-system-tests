// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

#include <util/Config.h>

#if defined(XR_USE_PLATFORM_XLIB) && defined(XR_USE_GRAPHICS_API_OPENGL)
#include <X11/Xlib.h>
#include <X11/Xutil.h>
/* avoid conflicts with catch2 */
#undef None
#undef Always
#undef Never
/* avoid conflicts with Eigen */
#undef Success
#include <glad/glx.h>
#endif // XR_USE_PLATFORM_XLIB

#ifdef XR_USE_GRAPHICS_API_OPENGL
#include <glad/gl.h>
#endif // XR_USE_GRAPHICS_API_OPENGL

#ifdef XR_USE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <Windows.h>
#endif

#include <openxr/openxr.h>
#include <openxr/openxr_platform.h>
