// Copyright 2018-2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Implementation
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#include "ResultCode.h"
#include <openxr/openxr_reflection.h>

#include <iostream>
#include <sstream>

std::ostream& operator<<(std::ostream& os, XrResultCode const& code)
{
#define MAKE_CASE(SYM, _)                                                                                              \
	case SYM:                                                                                                          \
		os << #SYM;                                                                                                    \
		return os;
	switch (code.result) {
		XR_LIST_ENUM_XrResult(MAKE_CASE);
	}
	std::ostringstream oss;
	if (code.result < 0) {
		oss << "XR_UNKNOWN_FAILURE_";
	} else {
		oss << "XR_UNKNOWN_SUCCESS_";
	}
	oss << code.result;
	os << oss.str();
	return os;
}
