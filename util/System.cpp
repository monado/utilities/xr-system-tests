// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Implementation
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#include "System.h"
#include "SystemTestUtils.h"
#include <xrtraits/InitXrType.h>

using xrtraits::Initialized;

XrSystemId formFactorToSystemId(XrInstance instance, XrFormFactor formFactor)
{
	XrSystemId systemId = XR_NULL_SYSTEM_ID;
	Initialized<XrSystemGetInfo> system_get_info{formFactor};
	REQUIRE_RESULT(XR_SUCCESS, xrGetSystem(instance, &system_get_info, &systemId));
	REQUIRE_FALSE(systemId == XR_NULL_SYSTEM_ID);
	return systemId;
}
