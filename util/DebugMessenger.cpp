// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Implementation
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#include "DebugMessenger.h"
#include "Helpers.h"

#include <catch2/catch.hpp>
#include <xrtraits/InitXrType.h>

#include <iomanip>
#include <iostream>
#include <sstream>

using xrtraits::Initialized;

#ifdef XR_EXT_debug_utils
enum class MessageSeverity : uint64_t
{
	Verbose,
	Info,
	Warning,
	Error
};
enum class MessageType : uint64_t
{
	General = 0x1,
	Validation = 0x10,
	Performance = 0x100
};
enum class ObjectType : uint64_t
{
	Unknown,
	Instance,
	Session,
	Swapchain,
	Space,
	ActionSet,
	Action,
	DebugUtilsMesssengerEXT
};

static const char* SYSTEST_INTERNAL_MESSAGE_ID = "SysTests";

struct ObjectInfo
{
	ObjectType objectType;
	uint64_t objectHandle;
	std::string objectName;
};
struct MessageData
{
	static MessageData fromOpenXR(uint64_t messageSeverity, uint64_t messageTypes,
	                              const XrDebugUtilsMessengerCallbackDataEXT& callbackData);
	MessageSeverity messageSeverity;
	uint64_t messageTypes;
	std::string messageId;
	std::string functionName;
	std::string message;
	std::vector<ObjectInfo> objects;
	std::vector<std::string> sessionLabels;
};
std::string to_string(MessageData const& data);

template <typename EnumClassType> static inline bool is_set(uint64_t bitfield, EnumClassType bit)
{
	return (bitfield & uint64_t(bit)) != 0;
}

static inline std::string message_type_to_string(uint64_t types)
{
	std::ostringstream oss;
	if (is_set(types, MessageType::General)) {
		oss << "G";
	}
	if (is_set(types, MessageType::Validation)) {
		oss << "V";
	}
	if (is_set(types, MessageType::Performance)) {
		oss << "P";
	}
	return oss.str();
}
static inline const char* to_cstring(MessageSeverity messageSeverity)
{
	switch (messageSeverity) {
	case MessageSeverity::Verbose:
		return "Verbose";
	case MessageSeverity::Info:
		return "Info";
	case MessageSeverity::Warning:
		return "Warning";
	case MessageSeverity::Error:
		return "Error";

	default:
		return "UNKNOWN SEVERITY";
	}
}

std::string to_string(MessageData const& data)
{
	std::ostringstream oss;
	oss << "[" << std::setw(3) << std::left << message_type_to_string(data.messageTypes) << "]";
	oss << "[" << std::setw(7) << std::left << to_cstring(data.messageSeverity) << "]";
	oss << "[" << std::setw(20) << std::left << data.messageId << "]";
	oss << "[" << std::setw(32) << std::left << data.functionName << "]";
	oss << " " << data.message;
	return oss.str();
}

static inline MessageSeverity convertSeverity(uint64_t severity)
{
	/// @todo is this really supposed to be a bit mask, not just an enum?
	switch (severity) {
	case XR_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT:
		return MessageSeverity::Verbose;
	case XR_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT:
		return MessageSeverity::Info;
	case XR_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT:
		return MessageSeverity::Warning;
	case XR_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:
		return MessageSeverity::Error;

	default:
		return MessageSeverity(0);
	}
}

static inline uint64_t convertMessageTypes(uint64_t xrMessageTypes)
{
	uint64_t ret = 0;
	if (is_set(xrMessageTypes, XR_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT)) {
		ret |= uint64_t(MessageType::General);
	}
	if (is_set(xrMessageTypes, XR_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT)) {
		ret |= uint64_t(MessageType::Validation);
	}
	if (is_set(xrMessageTypes, XR_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT)) {
		ret |= uint64_t(MessageType::Performance);
	}
	return ret;
}

MessageData MessageData::fromOpenXR(uint64_t messageSeverity, uint64_t messageTypes,
                                    const XrDebugUtilsMessengerCallbackDataEXT& callbackData)
{
	/// @todo convert objects and session labels.
	return MessageData{convertSeverity(messageSeverity),
	                   convertMessageTypes(messageTypes),
	                   callbackData.messageId,
	                   callbackData.functionName,
	                   callbackData.message,
	                   {},
	                   {}};
}

static XrBool32 XRAPI_CALL messengerCallback(XrDebugUtilsMessageSeverityFlagsEXT messageSeverity,
                                             XrDebugUtilsMessageTypeFlagsEXT messageTypes,
                                             const XrDebugUtilsMessengerCallbackDataEXT* callbackData,
                                             void* /* userData */)
{
	std::cout << to_string(MessageData::fromOpenXR(messageSeverity, messageTypes, *callbackData)) << std::endl;
	return XR_FALSE;
}

XrResult registerDebugMessenger(XrInstance instance, XrDebugUtilsMessageSeverityFlagsEXT messageSeverities,
                                XrDebugUtilsMessageTypeFlagsEXT messageTypes, XrDebugUtilsMessengerEXT& outMessenger)
{
	Initialized<XrDebugUtilsMessengerCreateInfoEXT> messengerCreateInfo{
	    messageSeverities, messageTypes, &messengerCallback, nullptr /* no userdata right now */
	};
	try {
		auto createMessenger = XRUTIL_GET_INST_PROC_ADDR(instance, xrCreateDebugUtilsMessengerEXT);
		auto result = createMessenger(instance, &messengerCreateInfo, &outMessenger);
		return result;
	} catch (std::exception const& e) {
		std::cerr << "Failed registering debug messenger: " << e.what() << "\n";
		return XR_ERROR_FUNCTION_UNSUPPORTED;
	}
}

XrResult submitDebugMessage(XrInstance instance, XrDebugUtilsMessageSeverityFlagsEXT messageSeverity,
                            XrDebugUtilsMessageTypeFlagsEXT messageType, const char* messageId,
                            const char* functionName, const char* message)
{
	try {
		auto submitMessage = XRUTIL_GET_INST_PROC_ADDR(instance, xrSubmitDebugUtilsMessageEXT);

		Initialized<XrDebugUtilsMessengerCallbackDataEXT> data{messageId, functionName, message, uint32_t(0),
		                                                       nullptr,   uint32_t(0),  nullptr};

		auto result = submitMessage(instance, messageSeverity, messageType, &data);

		return result;
	} catch (std::exception const& e) {
		std::cerr << "Failed submitting debug message: " << e.what() << "\n";
		return XR_ERROR_RUNTIME_FAILURE;
	}
}
#endif // XR_EXT_debug_utils
