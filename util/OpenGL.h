// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

#include <util/Config.h>

#ifdef XR_USE_GRAPHICS_API_OPENGL

#include "Session.h"

SessionInterfacePtr makeOpenGLSession(XrInstance instance, XrSystemId systemId);
SessionInterfacePtr makeOpenGLSessionWithFormFactor(XrInstance instance, XrFormFactor formFactor);

#endif // BUILD_OPENGL
