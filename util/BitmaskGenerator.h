// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header providing a way to name bitmask bits, combine names and descriptions,
 * and generate all combinations of bitmask bits.
 *
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

#include "Generator.h"

#include <initializer_list>
#include <string>

/*!
 * String description and value of a specific bit or collection of bits forming a bit flag/bitmask.
 */
struct BitmaskData
{
	std::string description;
	uint64_t bitmask;

	/// Is this an empty description and 0 bitmask?
	bool empty() const { return bitmask == 0 && description.empty(); }

	/// Update this bitmask with another via bitwise-or
	BitmaskData& operator|=(BitmaskData const& other);
};

/*!
 * Bitwise-OR operator for BitmaskData that combines the bits as well as the descriptions in a readable way.
 *
 * @relates BitmaskData
 */
BitmaskData operator|(BitmaskData const& lhs, BitmaskData const& rhs);

/*!
 * Generate all combinations of the supplied list of bitmasks,
 * including the 0 combination with none of the element (and thus bits).
 *
 * @see BitmaskData
 * @see bitmaskGenerator
 *
 * @ingroup Generators
 */
GeneratorWrapper<BitmaskData const&> bitmaskGeneratorIncluding0(std::initializer_list<BitmaskData> const& bits);

/*!
 * Generate all combinations of the supplied list of bitmasks that include at least one set element.
 *
 * This excludes the 0 combination.
 *
 * @see BitmaskData
 * @see bitmaskGeneratorIncluding0
 *
 * @ingroup Generators
 */
GeneratorWrapper<BitmaskData const&> bitmaskGenerator(std::initializer_list<BitmaskData> const& bits);
