// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

#include <memory>
#include <openxr/openxr.h>

class SessionInterface
{
public:
	virtual ~SessionInterface();

	/// Access the raw handle for the session.
	virtual XrSession get() const = 0;

	virtual XrSystemId getSystemId() const = 0;

protected:
	SessionInterface();
};
using SessionInterfacePtr = std::unique_ptr<SessionInterface>;
