// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

#include <memory>
#include <stdint.h>
#include <string>
#include <vector>

/// A container for a vector of strings that owns storage for them, and exposes an array of raw pointers.
///
/// This bridges the gap between OpenXR and other things that want arrays of pointers to c-strings,
/// and a vector of std::string in usability.
///
/// All strings supplied are copied.
struct StringVec
{
public:
	StringVec() = default;
	StringVec(StringVec&&) = default;
	StringVec& operator=(StringVec&&) = default;

	/// Copy-construct
	StringVec(StringVec const& other);

	/// Copy-assign
	StringVec& operator=(StringVec const& other);

	/// Construct from vector of std::string.
	StringVec(std::vector<std::string> const& other);

	/// Assign from vector of std::string.
	StringVec& operator=(std::vector<std::string> const& other);

	using inner = std::vector<const char*>;
	using const_iterator = typename inner::const_iterator;

	/// "Conversion" operator to the contained vector of C string pointers.
	operator inner const&() const { return strPtrVector; }

	const char* operator[](size_t i) const { return strPtrVector[i]; }
	bool empty() const { return strPtrVector.empty(); }
	uint32_t size() const { return static_cast<uint32_t>(strPtrVector.size()); }
	const char* const* data() const { return strPtrVector.data(); }
	const_iterator begin() const { return strPtrVector.begin(); }
	const_iterator end() const { return strPtrVector.end(); }

	// Returns true if the string exists in this vector (case-sensitive)
	bool contains(std::string const& str) const;

	void push_back(const char* str);
	void push_back(std::string const& str) { push_back(str.c_str()); }

	// Adds the specified string to the container only if it does not already exist (case-sensitive).
	void push_back_unique(std::string const& str);
	void push_back_unique(const char* str);

	void set(size_t i, const char* str);
	void set(size_t i, std::string const& str) { set(i, str.c_str()); }

	void erase(const_iterator it);

	void clear();

private:
	static std::unique_ptr<char[]> copyString(const char* str);
	void rebuild();
	std::vector<std::unique_ptr<char[]>> strOwnVector;
	inner strPtrVector;
};
