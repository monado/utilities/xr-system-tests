// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Implementation
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#include "SessionGenerator.h"
#include "GraphicsSupport.h"
#include "Headless.h"
#include "Instance.h"
#include "OpenGL.h"
#include "System.h"
#include "XrPlatformInclude.h"

#include <algorithm>
#include <assert.h>
#include <deque>
#include <functional>
#include <memory>

SessionEtAlPtr SessionEtAlCreator::invoke(XrFormFactor ff) const
{
	auto ret = std::make_unique<SessionEtAl>(details, makeInstance({}, extensions));
	auto sys = formFactorToSystemId(ret->getInstance(), ff);
	ret->session = sessionCreator(ret->getInstance(), sys);
	// safety check
	assert(sys == ret->session->getSystemId());
	return ret;
}

namespace {
// using SessionEtAlCreator = std::function<void(SessionEtAlPtr, XrFormFactor)>;

using CreatorList = std::deque<const SessionEtAlCreator*>;

class SessionGenerator : public GeneratorBase<SessionEtAlCreator const&>
{
public:
	~SessionGenerator() override = default;
	static std::unique_ptr<GeneratorBase<SessionEtAlCreator const&>> create(CreatorList&& creators)
	{
		return std::make_unique<SessionGenerator>(std::move(creators));
	}
	SessionGenerator(CreatorList&& creators) : creators_(std::move(creators)) {}
	SessionEtAlCreator const& get() override { return *current_; }
	bool next() override
	{
		if (creators_.empty()) {
			return false;
		}
		current_ = creators_.front();
		creators_.pop_front();
		return true;
	}

private:
	CreatorList creators_;
	const SessionEtAlCreator* current_;
};
} // namespace

SessionEtAl::SessionEtAl(SessionDetails dets, InstanceEtAlPtr&& inst) : instance(std::move(inst)), details(dets) {}

#ifdef XR_USE_GRAPHICS_API_OPENGL
#ifdef XR_USE_PLATFORM_XLIB
static const SessionEtAlCreator OpenGLviaXlib{{"OpenGL via xlib", GraphicsApi::OpenGL}, // session details metadata
                                              {XR_KHR_OPENGL_ENABLE_EXTENSION_NAME},
                                              makeOpenGLSession};
#endif // XR_USE_PLATFORM_XLIB
#endif // XR_USE_GRAPHICS_API_OPENGL

static const SessionEtAlCreator MndHeadless{{"Monado Headless", GraphicsApi::Headless}, // session details metadata
                                            {XR_MND_HEADLESS_EXTENSION_NAME},
                                            makeHeadlessSession};

//! Populate a creator list with all the graphical session creators, shared between generators.
static CreatorList makeGraphicalCreatorList()
{
	auto const& supported = GraphicsSupport::get();
	//! @todo cache these lists at startup and just iterate through them, rather than consume them, in the
	//! generators. Should improve performance substantially.
	CreatorList creators;

	if (supported.opengl) {
#ifdef XR_USE_PLATFORM_XLIB
		creators.push_back(&OpenGLviaXlib);
#endif
	}
	if (supported.vulkan) {
		//! @todo
	}
	return creators;
}

GeneratorWrapper<SessionEtAlCreator const&> sessionGenerator()
{
	auto creators = makeGraphicalCreatorList();
	if (GraphicsSupport::get().mnd_headless) {
		creators.push_back(&MndHeadless);
	}
	return {SessionGenerator::create(std::move(creators))};
}

GeneratorWrapper<SessionEtAlCreator const&> graphicalSessionGenerator()
{
	return {SessionGenerator::create(makeGraphicalCreatorList())};
}

GeneratorWrapper<SessionEtAlCreator const&> headlessSessionGenerator()
{
	return {SessionGenerator::create({&MndHeadless})};
}
