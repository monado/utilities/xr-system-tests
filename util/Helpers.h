// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

#include <openxr/openxr.h>

namespace xrutil {

template <typename T> T getInstProcAddr(XrInstance instance, const char* name)
{
	T ptr = nullptr;
	auto result = xrGetInstanceProcAddr(instance, name, reinterpret_cast<PFN_xrVoidFunction*>(&ptr));
	if (!XR_UNQUALIFIED_SUCCESS(result)) {
		return nullptr;
	}
	return ptr;
}

} // namespace xrutil

#define XRUTIL_GET_INST_PROC_ADDR(INSTANCE, NAME) (xrutil::getInstProcAddr<PFN_##NAME>(instance, #NAME))
