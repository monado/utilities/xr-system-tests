// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Implementation
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#include "Handles.h"
#include "Helpers.h"
#include "ResultCode.h"

#include <catch2/catch.hpp>

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

namespace testing_handle_destroyers {
DestroyDebugUtilsMessengerEXT::DestroyDebugUtilsMessengerEXT(XrInstance instance)
    : pfn_(XRUTIL_GET_INST_PROC_ADDR(instance, xrDestroyDebugUtilsMessengerEXT))
{}

void DestroyDebugUtilsMessengerEXT::operator()(XrDebugUtilsMessengerEXT handle) const
{
	if (handle != XR_NULL_HANDLE_CPP) {
		XrResult result = pfn_(handle);
		if (XR_SUCCESS != result) {
			FAIL("Failed destroying a DebugUtilsMessengerEXT:" << XrResultCode{result});
		}
	}
}

// Because we explicitly defined it above.
#define XRUTIL_HANDLE_SKIP_DEBUGUTILSMESSENGEREXT
#define XRUTIL_HANDLE_TYPE(HandleName)                                                                                 \
	void Destroy##HandleName::operator()(Xr##HandleName handle) const                                                  \
	{                                                                                                                  \
		if (handle != XR_NULL_HANDLE_CPP) {                                                                            \
			XrResult result = xrDestroy##HandleName(handle);                                                           \
			if (XR_SUCCESS != result) {                                                                                \
				FAIL("Failed destroying a " << #HandleName << XrResultCode{result});                                   \
			}                                                                                                          \
		}                                                                                                              \
	}

#include "ForEachHandleType.h"
} // namespace testing_handle_destroyers

std::ostream& operator<<(std::ostream& os, NullHandleType const&)
{
	os << "XR_NULL_HANDLE";
	return os;
}
