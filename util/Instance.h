// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

#include "Handles.h"

#include <initializer_list>
#include <memory>
#include <vector>

class InstanceEtAl
{
public:
	/// Construct from an instance and an (optional) debug messenger.
	InstanceEtAl(ScopedInstance&& inst, XrDebugUtilsMessengerEXT mssngr);
	~InstanceEtAl();

	/// Access the raw handle for the instance.
	XrInstance getInstance() const { return instance.get(); }

private:
	ScopedInstance instance;
	ScopedDebugUtilsMessengerEXT messenger;
};
using InstanceEtAlPtr = std::unique_ptr<InstanceEtAl>;
InstanceEtAlPtr makeInstance(std::initializer_list<const char*> const& layers,
                             std::initializer_list<const char*> const& extensions);

InstanceEtAlPtr makeInstance(std::initializer_list<const char*> const& layers,
                             std::vector<const char*> const& extensions);

// Instance-related helpers

bool hasApiLayer(std::vector<XrApiLayerProperties> const& apiLayers, const char* name);

void conditionallyAddApiLayer(std::vector<XrApiLayerProperties> const& apiLayers, const char* name,
                              std::vector<const char*>& out_enabled);

bool hasExtension(std::vector<XrExtensionProperties> const& extensions, const char* name);
