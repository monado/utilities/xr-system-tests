// Copyright 2018-2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

#include <openxr/openxr.h>

#include <iosfwd>

struct XrResultCode
{
	XrResultCode() = default;
	XrResultCode(XrResult res) : result(res) {}
	XrResult result;
};

inline bool operator==(XrResultCode const& lhs, XrResultCode const& rhs) { return lhs.result == rhs.result; }

inline bool operator==(XrResultCode const& lhs, XrResult const& rhs) { return lhs.result == rhs; }

std::ostream& operator<<(std::ostream& os, XrResultCode const& code);
