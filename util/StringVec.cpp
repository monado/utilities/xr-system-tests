// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Implementation
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#include "StringVec.h"

#include <algorithm>
#include <cstring>

StringVec::StringVec(StringVec const& other)
{
	for (auto& str : other) {
		push_back(str);
	}
}

StringVec::StringVec(std::vector<std::string> const& other)
{
	for (auto& str : other) {
		push_back(str);
	}
}

StringVec& StringVec::operator=(StringVec const& other)
{
	if (this == &other) {
		// self-assign
		return *this;
	}
	// copy
	auto newVec = StringVec{other};

	// swap
	std::swap(strOwnVector, newVec.strOwnVector);
	std::swap(strPtrVector, newVec.strPtrVector);
	return *this;
}

StringVec& StringVec::operator=(std::vector<std::string> const& other)
{
	// copy
	auto newVec = StringVec{other};

	// swap
	std::swap(strOwnVector, newVec.strOwnVector);
	std::swap(strPtrVector, newVec.strPtrVector);
	return *this;
}

std::unique_ptr<char[]> StringVec::copyString(const char* str)
{
	auto len = strlen(str) + 1;
	std::unique_ptr<char[]> copy(new char[len]);
	std::memcpy(copy.get(), str, len);
	return copy;
}

bool StringVec::contains(std::string const& str) const
{
	const auto e = end();
	auto it = std::find(begin(), e, str);
	return it != e;
}

void StringVec::push_back(const char* str)
{
	// Copy the string
	auto copy = copyString(str);
	auto raw = copy.get();
	// Store the unique_ptr that owns the copy, as well as the raw pointer.
	strOwnVector.emplace_back(std::move(copy));
	strPtrVector.push_back(raw);
}

void StringVec::push_back_unique(std::string const& str)
{
	if (!contains(str)) {
		push_back(str);
	}
}

void StringVec::push_back_unique(const char* str)
{
	if (!contains(str)) {
		push_back(str);
	}
}

void StringVec::set(size_t i, const char* str)
{
	if (i > strPtrVector.size()) {
		throw std::out_of_range("out of range when setting string");
	}
	auto copy = copyString(str);
	auto raw = copy.get();
	strOwnVector[i] = std::move(copy);
	strPtrVector[i] = raw;
}

void StringVec::clear()
{
	strPtrVector.clear();
	strOwnVector.clear();
}

void StringVec::erase(const_iterator it)
{
	auto i = std::distance(begin(), it);
	{
		auto it2 = strPtrVector.begin() + i;
		strPtrVector.erase(it2);
	}
	{
		auto it2 = strOwnVector.begin() + i;
		strOwnVector.erase(it2);
	}
}

void StringVec::rebuild()
{
	strPtrVector.clear();
	for (const auto& ptr : strOwnVector) {
		strPtrVector.push_back(ptr.get());
	}
}
