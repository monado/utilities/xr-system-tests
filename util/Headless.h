// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

#include "Session.h"

SessionInterfacePtr makeHeadlessSession(XrInstance instance, XrSystemId systemId);
SessionInterfacePtr makeHeadlessSessionWithFormFactor(XrInstance instance, XrSystemId systemId);
