// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

struct GraphicsSupport
{
	GraphicsSupport();
	bool mnd_headless;
	bool opengl;
	bool vulkan;
	static GraphicsSupport const& get();
};
