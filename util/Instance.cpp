// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Implementation
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#include "Instance.h"
#include "DebugMessenger.h"
#include "Helpers.h"
#include "Options.h"
#include "ResultCode.h"
#include "StringVec.h"

#include <xrtraits/InitXrType.h>
#include <xrtraits/TwoCall.h>

#include <catch2/catch.hpp>

#include <algorithm>
#include <iostream>
#include <string>

using namespace std::string_literals;
using xrtraits::doTwoCallInPlace;
using xrtraits::Initialized;

bool hasApiLayer(std::vector<XrApiLayerProperties> const& apiLayers, const char* name)
{
	auto b = apiLayers.begin();
	auto e = apiLayers.end();

	const std::string name_string = name;

	return (e != std::find_if(b, e, [&](const auto& prop) { return prop.layerName == name_string; }));
}

void conditionallyAddApiLayer(std::vector<XrApiLayerProperties> const& apiLayers, const char* name,
                              StringVec& out_enabled)
{
	if (hasApiLayer(apiLayers, name)) {
		// OK we have this layer.
		out_enabled.push_back_unique(name);
	}
}

bool hasExtension(std::vector<XrExtensionProperties> const& extensions, const char* name)
{
	auto b = extensions.begin();
	auto e = extensions.end();

	const std::string name_string = name;

	return (e != std::find_if(b, e, [&](const auto& prop) { return prop.extensionName == name_string; }));
}

namespace inner {
template <typename LayerNameCollection, typename ExtensionNameCollection>
inline XrResult makeInstance(ScopedInstance& out, XrDebugUtilsMessengerEXT& out_messenger,
                             LayerNameCollection const& layers, ExtensionNameCollection const& extensions)
{
	// Configure API layers
	std::vector<XrApiLayerProperties> availableLayers;
	{
		auto result = doTwoCallInPlace(availableLayers, xrEnumerateApiLayerProperties);
		if (!XR_SUCCEEDED(result)) {
			return result;
		}
	}
	StringVec enabledLayers;
	for (const auto& name : layers) {
		if (hasApiLayer(availableLayers, name)) {
			enabledLayers.push_back_unique(name);
		} else {
			FAIL("Requested layer not available: " << name);
		}
	}

#ifdef FORCE_API_DUMP
	conditionallyAddApiLayer(availableLayers, "XR_APILAYER_LUNARG_api_dump", enabledLayers);
#endif // FORCE_API_DUMP
#ifdef FORCE_VALIDATION
	conditionallyAddApiLayer(availableLayers, "XR_APILAYER_LUNARG_core_validation", enabledLayers);
#endif // FORCE_VALIDATION

	// Configure extensions
	std::vector<XrExtensionProperties> availableExtensions;
	{
		auto result = doTwoCallInPlace(availableExtensions, xrEnumerateInstanceExtensionProperties, nullptr);
		if (!XR_SUCCEEDED(result)) {
			return result;
		}
	}
	StringVec enabledExtensions;
	for (const auto& name : extensions) {
		if (hasExtension(availableExtensions, name)) {
			enabledExtensions.push_back_unique(name);
		} else {
			throw std::runtime_error("Requested extension not available: "s + name);
		}
	}

	// If debug is not disabled, and the runtime/loader reports it is available, then enable it.
	const bool useDebug =
	    Options::get().useDebugMessenger && hasExtension(availableExtensions, XR_EXT_DEBUG_UTILS_EXTENSION_NAME);
	if (useDebug) {
		enabledExtensions.push_back_unique(XR_EXT_DEBUG_UTILS_EXTENSION_NAME);
	}

	Initialized<XrInstanceCreateInfo> instanceCreateInfo{
	    XrInstanceCreateFlags(0),
	    XrApplicationInfo{"System Tests", 1, // app version
	                      "", 0,             // engine version
	                      XR_CURRENT_API_VERSION},
	    enabledLayers.size(),
	    enabledLayers.data(),
	    enabledExtensions.size(),
	    enabledExtensions.data(),
	};
	auto result = xrCreateInstance(&instanceCreateInfo, out.resetAndGetAddress());
	if (!XR_SUCCEEDED(result)) {
		// Early out on failure.
		return result;
	}

	if (useDebug) {
		out_messenger = XR_NULL_HANDLE_CPP;
		auto debug_result = registerDebugMessenger(
		    out.get(),
		    XR_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT | XR_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
		        XR_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT, /* skip verbose messages */

		    XR_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
		        XR_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT, /* skip performance messages? */

		    out_messenger);
		if (!XR_SUCCEEDED(debug_result)) {
			std::cerr << "Tried but failed to register debug messenger: " << debug_result << ".\n";
		}
		submitDebugMessage(out.get(), XR_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT,
		                   XR_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT, "", __func__,
		                   "Initial test message coming from inside the app.");
	}

	return result;
}
} // namespace inner

InstanceEtAl::InstanceEtAl(ScopedInstance&& inst, XrDebugUtilsMessengerEXT mssngr)
    : instance(std::move(inst)), messenger(mssngr, instance.get())
{}
InstanceEtAl::~InstanceEtAl() {}

InstanceEtAlPtr makeInstance(std::initializer_list<const char*> const& layers,
                             std::initializer_list<const char*> const& extensions)
{
	ScopedInstance instance;
	XrDebugUtilsMessengerEXT messenger = XR_NULL_HANDLE_CPP;
	auto result = inner::makeInstance(instance, messenger, layers, extensions);
	if (result != XR_SUCCESS) {
		FAIL("Could not create instance, got " << XrResultCode(result));
	}
	if (instance == XR_NULL_HANDLE_CPP) {
		FAIL("xrCreateInstance claimed success but returned a null handle.");
	}
	return std::make_unique<InstanceEtAl>(std::move(instance), messenger);
}

InstanceEtAlPtr makeInstance(std::initializer_list<const char*> const& layers,
                             std::vector<const char*> const& extensions)
{
	ScopedInstance instance;
	XrDebugUtilsMessengerEXT messenger = XR_NULL_HANDLE_CPP;
	auto result = inner::makeInstance(instance, messenger, layers, extensions);
	if (!XR_UNQUALIFIED_SUCCESS(result)) {
		throw std::logic_error("Could not create instance.");
	}
	if (instance == XR_NULL_HANDLE_CPP) {
		throw std::logic_error("xrCreateInstance claimed success but returned a null handle.");
	}
	return std::make_unique<InstanceEtAl>(std::move(instance), messenger);
}
