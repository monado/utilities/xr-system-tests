// Copyright 2018, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

#include <openxr/openxr.h>

#include <catch2/catch.hpp>

#include <util/CatchPredicates.h>
#include <util/Handles.h>
#include <util/ResultCode.h>

#include <gsl/gsl>

#include <xrtraits/InitXrType.h>

// Standard Includes
#include <algorithm>
#include <functional>
#include <initializer_list>
#include <memory>
#include <sstream>
#include <stdexcept>
#include <stdlib.h>
#include <vector>

using xrtraits::Initialized;
using xrtraits::make_zeroed;
using xrtraits::make_zeroed_vector;

/// Namespace of implementation details
namespace twocallimpl {

/// Class turning a variadic list of string literals (each argument to a CHECK_TWO_CALL macro) into the basic pieces
/// used in the two call checker assertion messages.
struct Strings
{
	template <typename... Args> Strings(const char* typeName, const char* callName, Args... a)
	{
		Catch::ReusableStringStream callStartStream;
		callStartStream << callName << "( ";

		Catch::ReusableStringStream exprStream;
		exprStream << typeName << ", " << callName;

		std::initializer_list<const char*> extraArgs{std::forward<Args>(a)...};
		if (extraArgs.size() != 0) {
			for (const auto& arg : extraArgs) {
				callStartStream << arg << ", ";
				exprStream << ", " << arg;
			}
		}
		expressionString = exprStream.str();
		callStart = callStartStream.str();
	}
	std::string expressionString;
	std::string callStart;
};

/// Main workings of the two-call checker.
template <typename T, typename F, typename... Args>
inline std::vector<T> test(Catch::StringRef const& macroName, Strings const& strings,
                           const Catch::SourceLineInfo& lineinfo, Catch::ResultDisposition::Flags resultDisposition,
                           F&& wrappedCall, Args&&... a)
{
	std::vector<T> ret;
	uint32_t count = 0;
	{
		std::string name = (Catch::ReusableStringStream() << strings.expressionString << " ) // count request call: "
		                                                  << strings.callStart << "0, &count, nullptr")
		                       .str();

		Catch::AssertionHandler catchAssertionHandler(macroName, lineinfo, name, resultDisposition);
		INTERNAL_CATCH_TRY
		{
			auto result = wrappedCall(std::forward<Args>(a)..., 0, &count, nullptr);
			catchAssertionHandler.handleExpr(Catch::ExprLhs<XrResultCode>(XR_SUCCESS) == XrResultCode(result));
		}
		INTERNAL_CATCH_CATCH(catchAssertionHandler)
		INTERNAL_CATCH_REACT(catchAssertionHandler)
	}

	if (Catch::getResultCapture().lastAssertionPassed() && count > 0) {
		std::string name = (Catch::ReusableStringStream()
		                    << strings.expressionString << " ) // buffer fill call: " << strings.callStart << count
		                    << " /*capacity*/, &count, array")
		                       .str();

		Catch::AssertionHandler catchAssertionHandler(macroName, lineinfo, name, resultDisposition);
		INTERNAL_CATCH_TRY
		{
			// Allocate
			ret.resize(count, make_zeroed<T>());

			// Perform call and handle assertion
			auto result = wrappedCall(std::forward<Args>(a)..., uint32_t(ret.size()), &count, ret.data());
			catchAssertionHandler.handleExpr(Catch::ExprLhs<XrResultCode>(XR_SUCCESS) == XrResultCode(result));
			if (Catch::getResultCapture().lastAssertionPassed()) {
				// If success, resize to exact length.
				ret.resize(count);
			} else {
				// In case of error, clear return value
				ret.clear();
			}
		}
		INTERNAL_CATCH_CATCH(catchAssertionHandler)
		INTERNAL_CATCH_REACT(catchAssertionHandler)
	}
	return ret;
}
} // namespace twocallimpl

// Internal macro
#define INTERNAL_TWO_CALL_STRINGIFY(...)                                                                               \
	twocallimpl::Strings { CATCH_REC_LIST(CATCH_INTERNAL_STRINGIFY, __VA_ARGS__) }

// Internal macro providing shared implementation between CHECK_TWO_CALL and REQUIRE_TWO_CALL
#define INTERNAL_TEST_TWO_CALL(macroName, resultDisposition, STRINGS, TYPE, ...)                                       \
	[&] {                                                                                                              \
		return twocallimpl::test<TYPE>(macroName##_catch_sr, STRINGS, CATCH_INTERNAL_LINEINFO, resultDisposition,      \
		                               __VA_ARGS__);                                                                   \
	}()

/*!
 * @defgroup TwoCallCheckers Checkers for Two Call Idiom
 *
 * All of these perform the two call idiom and return a std::vector<T>. Arguments are:
 *
 * - The type of a single buffer element
 * - The name of the call
 * - Any additional arguments that should be passed **before**  the capacityInput, countOutput, and array parameters
 */
/// @{

#ifndef CATCH_CONFIG_TRADITIONAL_MSVC_PREPROCESSOR
/// Try a two-call idiom in "check" mode: failures are recorded but return an empty container.
#define CHECK_TWO_CALL(TYPE, ...)                                                                                      \
	INTERNAL_TEST_TWO_CALL("CHECK_TWO_CALL", Catch::ResultDisposition::ContinueOnFailure,                              \
	                       (INTERNAL_TWO_CALL_STRINGIFY(TYPE, __VA_ARGS__)), TYPE, __VA_ARGS__)
/// Try a two-call idiom in "require" mode: failures are recorded and terminate the execution.
#define REQUIRE_TWO_CALL(TYPE, ...)                                                                                    \
	INTERNAL_TEST_TWO_CALL("REQUIRE_TWO_CALL", Catch::ResultDisposition::Normal,                                       \
	                       (INTERNAL_TWO_CALL_STRINGIFY(TYPE, __VA_ARGS__)), TYPE, __VA_ARGS__)
#else
/// Try a two-call idiom in "check" mode: failures are recorded but return an empty container.
#define CHECK_TWO_CALL(TYPE, ...)                                                                                      \
	INTERNAL_TEST_TWO_CALL("CHECK_TWO_CALL", Catch::ResultDisposition::ContinueOnFailure,                              \
	                       INTERNAL_CATCH_EXPAND_VARGS(INTERNAL_TWO_CALL_STRINGIFY(TYPE, __VA_ARGS__)), TYPE,          \
	                       __VA_ARGS__)
/// Try a two-call idiom in "require" mode: failures are recorded and terminate the execution.
#define REQUIRE_TWO_CALL(TYPE, ...)                                                                                    \
	INTERNAL_TEST_TWO_CALL("REQUIRE_TWO_CALL", Catch::ResultDisposition::Normal,                                       \
	                       INTERNAL_CATCH_EXPAND_VARGS(INTERNAL_TWO_CALL_STRINGIFY(TYPE, __VA_ARGS__)), TYPE,          \
	                       __VA_ARGS__)
#endif
/// @}

/*!
 * @defgroup ResultCheckers Checkers for XrResult
 *
 * These all wrap and resemble the corresponding assertion macros that lack the `_RESULT` piece. Expected return value
 * as well as the result of the expression are wrapped in a custom type that performs decoding of XrResult values to
 * strings for output. Arguments are:
 *
 * - The expected XrResult value
 * - The name of the function
 * - The arguments to the function, parenthesized
 */
/// @{

#define CHECK_RESULT(EXPECTED, EXPR) CHECK(XrResultCode{EXPECTED} == XrResultCode{EXPR})
#define REQUIRE_RESULT(EXPECTED, EXPR) REQUIRE(XrResultCode{EXPECTED} == XrResultCode{EXPR})
#define CHECK_RESULT_FALSE(EXPECTED, EXPR) CHECK_FALSE(XrResultCode{EXPECTED} == XrResultCode{EXPR})
#define REQUIRE_RESULT_FALSE(EXPECTED, EXPR) REQUIRE_FALSE(XrResultCode{EXPECTED} == XrResultCode{EXPR})

/// @}
