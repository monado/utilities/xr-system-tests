// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

#include <openxr/openxr.h>

#ifdef XR_EXT_debug_utils

XrResult registerDebugMessenger(XrInstance instance, XrDebugUtilsMessageSeverityFlagsEXT messageSeverities,
                                XrDebugUtilsMessageTypeFlagsEXT messageTypes, XrDebugUtilsMessengerEXT& outMessenger);

XrResult submitDebugMessage(XrInstance instance, XrDebugUtilsMessageSeverityFlagsEXT messageSeverity,
                            XrDebugUtilsMessageTypeFlagsEXT messageType, const char* messageId,
                            const char* functionName, const char* message);

#endif // XR_EXT_debug_utils
